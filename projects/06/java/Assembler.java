/**
 * Proudly Written By Itai Shalom and Yonatan Tahan
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class Assembler {
	public enum CommandType {
		C_COMMAND, A_COMMAND, L__COMMAND, NO_COMMAND
	}

	/**
	 * The main function
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException

	{
		File file = new File(args[0]);

		if (file.isFile()){
			driver( new BufferedReader(new FileReader(file)),new BufferedReader(new FileReader(file)),file.getAbsolutePath());
		}else{

			File[] listOfFiles = file.listFiles();

			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile() && listOfFiles[i].getAbsolutePath().endsWith(".asm")) {
					driver( new BufferedReader(new FileReader(listOfFiles[i])),new BufferedReader(new FileReader(listOfFiles[i])),listOfFiles[i].getAbsolutePath());
				} 
			}
		}
	}
	/**
	 * driver - Translates the asm file to hack file
	 * @param br1 - Buffer reader to read at the input file first
	 * @param br2 - Buffer reader to read at the input file first
	 * @param sFileName - File name
	 * @throws IOException
	 */
	private static void driver(BufferedReader br1,BufferedReader br2, String sFileName) throws IOException
	{
		Parser myParser = new Parser(br1);
		SymbolTable sT= new SymbolTable();
		fillPreDefinedSymbols(sT);
		int commandCounter=0;
		do
		{
			myParser.advance();
			CommandType command = myParser.commandType();
			switch (command)
			{
			case C_COMMAND:
				commandCounter++;
				break;
			case A_COMMAND:
				
				commandCounter++;
				break;
			case L__COMMAND:
			
				System.out.println(myParser.symbol());
				sT.addEntry(myParser.symbol(), commandCounter);
				break;
			default:
				continue;
			}
		}
		while (myParser.hasMoreCommands());
		br1.close();
		Parser myParser2 = new Parser(br2);
		String newFile = sFileName.replace("asm", "hack");
		PrintWriter writer = new PrintWriter(newFile, "UTF-8");
		int variableCounter=16;
		String sSymbol="";
		do 
		{
			myParser2.advance();
			CommandType command = myParser2.commandType();
			switch (command)
			{
			case C_COMMAND:
				writer.println("1"+Code.comp(myParser2.comp())+Code.dest(myParser2.dest())+Code.jump(myParser2.jump()));
				break;
			case A_COMMAND:
				sSymbol = myParser2.symbol();
				if ( isNumeric(sSymbol))
				{
					writer.println(SymbolTable.completeTo16Bit(Integer.toBinaryString(Integer.parseInt(sSymbol))));
				}
				else
				{
					if(sT.contains(sSymbol))
					{
						System.out.println(sT.getAddress(sSymbol));
						writer.println(sT.getAddress(sSymbol));
					}
					else
					{
						sT.addEntry(sSymbol, variableCounter);
						writer.println(sT.getAddress(sSymbol));
						variableCounter++;
					}
				}

				break;
			default:
				continue;
			}
		}while(myParser2.hasMoreCommands());
		br2.close();
		writer.close();
	}

	public static boolean isNumeric(String str)  
	{  
		try  
		{  
			Integer.parseInt(str);  
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}

	/**
	 * Fills the symbol table with constants variables.
	 * @param sT - The symbol table to fill
	 */
	private static void fillPreDefinedSymbols(SymbolTable sT) {
		sT.addEntry("SP", 0);
		sT.addEntry("LCL", 1);
		sT.addEntry("ARG", 2);
		sT.addEntry("THIS", 3);
		sT.addEntry("THAT", 4);
		sT.addEntry("SCREEN", 16384);
		sT.addEntry("KBD", 24576);
		for(int i=0;i<16;i++)
		{
			sT.addEntry("R"+i, i);
		}
	}

}
