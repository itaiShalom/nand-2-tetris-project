/**
 * Proudly Written By Itai Shalom and Yonatan Tahan
 */
package nana2tetrisP6;

public class Code {

	/**
	 * parse the dest part of a c-instruction in hack assembly language to the dest part
	 * of the c-instruction in hack machine language
	 * @param dest the dest part of a c-instruction
	 * @return string of length 3 contains 1's and 0's represent the dest
	 * bits in the c-instruction, according the input and the hack language
	 * specification
	 */
	public static String dest(String dest) {
		String result = null;
		if(dest==null){
			result = "000";
		}else {
			switch (dest) {
			case "M":
				result = "001";
				break;
			case "D":
				result = "010";
				break;
			case "MD":
				result = "011";
				break;
			case "A":
				result = "100";
				break;
			case "AM":
				result = "101";
				break;
			case "AD":
				result = "110";
				break;
			case "AMD":
				result = "111";
				break;
			default:
				break;
			}
		}
		return result;
	}
	
	/**
	 * parse the comp part of a c-instruction in hack assembly language to the comp part
	 * of the c-instruction in hack machine language
	 * @param comp the comp part of a c-instruction
	 * @return string of length 9 contains 1's and 0's represent the comp
	 * bits in the c-instruction, according the input and the hack language
	 * specification
	 */
	public static String comp(String comp) {
		String result = null;
		switch (comp) {
		case "0":
			result = "110101010";
			break;
		case "1":
			result = "110111111";
			break;
		case "-1":
			result = "110111010";
			break;
		case "D":
			result = "110001100";
			break;
		case "A":
			result = "110110000";
			break;
		case "!D":
			result = "110001101";
			break;
		case "!A":
			result = "110110001";
			break;
		case "-D":
			result = "110001111";
			break;
		case "-A":
			result = "110110011";
			break;
		case "D+1":
			result = "110011111";
			break;
		case "A+1":
			result = "110110111";
			break;
		case "D-1":
			result = "110001110";
			break;
		case "A-1":
			result = "110110010";
			break;
		case "D+A":
			result = "110000010";
			break;
		case "D-A":
			result = "110010011";
			break;
		case "A-D":
			result = "110000111";
			break;
		case "D&A":
			result = "110000000";
			break;
		case "D|A":
			result = "110010101";
			break;
		case "M":
			result = "111110000";
			break;
		case "!M":
			result = "111110001";
			break;
		case "-M":
			result = "111110011";
			break;
		case "M+1":
			result = "111110111";
			break;
		case "M-1":
			result = "111110010";
			break;
		case "D+M":
			result = "111000010";
			break;
		case "D-M":
			result = "111010011";
			break;
		case "M-D":
			result = "111000111";
			break;
		case "D&M":
			result = "111000000";
			break;
		case "D|M":
			result = "111010101";
			break;
		case "D<<":
			result = "101011000";
			break;
		case "D>>":
			result = "101001000";
			break;
		case "A<<":
			result = "101010000";
			break;
		case "A>>":
			result = "101000000";
			break;
		case "M<<":
			result = "101110000";
			break;
		case "M>>":
			result = "101100000";
			break;						
		default:
			break;
		}
		return result;
	}
	
	/**
	 * parse the jump part of a c-instruction in hack assembly language to the jump part
	 * of the c-instruction in hack machine language
	 * @param jump the jump part of a c-instruction
	 * @return string of length 3 contains 1's and 0's represent the jump
	 * bits in the c-instruction, according the input and the hack language
	 * specification
	 */
	public static String jump(String jump) {
		String result = null;
		if(jump == null){
			result = "000";
		}else {
			switch (jump) {
			case "JGT":
				result = "001";
				break;
			case "JEQ":
				result = "010";
				break;
			case "JGE":
				result = "011";
				break;
			case "JLT":
				result = "100";
				break;
			case "JNE":
				result = "101";
				break;
			case "JLE":
				result = "110";
				break;
			case "JMP":
				result = "111";
				break;
			default:
				break;
			}
		}
		return result;
	}
}
