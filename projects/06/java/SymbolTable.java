/**
 * Proudly Written By Itai Shalom and Yonatan Tahan
 */
import java.util.*;

public class SymbolTable {
	private Map<String,String> Table; 
	private final static int FULL_LENGTH = 16;

	/**
	 * Constructor of the SymbolTable
	 */
	public SymbolTable() {
		Table= new HashMap<String,String >();
	}

	/**
	 * addEntry - adds a new entry to the map
	 * @param symbol - The key
	 * @param address - The value
	 */
	public void addEntry(String symbol,int address)
	{
		String binaryRep = Integer.toBinaryString(address);	

		Table.put(symbol, completeTo16Bit(binaryRep));
	}

	/**
	 * contains - Checks if the key symbol exists at the table
	 * @param symbol - The key
	 * @return - True if key found, else false.
	 */
	public boolean contains(String symbol)
	{
		return Table.containsKey(symbol);
	}


/**
 * getAddress - Return the value for a given symbol key
 * @param symbol - The key
 * @return - The value if found
 */
	public String getAddress(String symbol)
	{
		return Table.get(symbol);
	}

	/**
	 * completeTo16Bit - Gets an number and adds zeros to the beginning of the binary number to be
	 * a 16 bit
	 * @param add - The binary number
	 * @return - The binary number 16 bit
	 */
	public static String completeTo16Bit(String add)
	{

		int length = add.length();
		int finalLenght = FULL_LENGTH-length;
		String temp = "";
		while (finalLenght>0)
		{
			temp +="0";
			finalLenght--;
		}

		for (int i=0;i<add.length();i++)

		{
			temp+=add.charAt(i);
		}
		return temp;
	}

}
