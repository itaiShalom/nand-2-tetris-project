/**
 * Proudly Written By Itai Shalom and Yonatan Tahan
 */

import java.io.BufferedReader;
import java.io.IOException;


public class Parser {
	private String comp, dest, jump;
	private String currLine,nextLine;
	private BufferedReader buffer;
	/**
	 * create a parser from a given BufferedReader
	 * @param asmFileBuffer a given BufferedReader
	 * @throws IOException
	 */
	public Parser(BufferedReader asmFileBuffer) throws IOException{
		currLine = null;
		comp = null;
		dest = null;
		jump = null;
		buffer = asmFileBuffer;
		nextLine=buffer.readLine();
	}
	
	/**
	 * checks if the .asm file have more commands
	 * @return true iff the file has more commands
	 * @throws IOException
	 */
	public boolean hasMoreCommands() throws IOException {
		return ((nextLine=buffer.readLine()) !=null);
	}
	
	/**
	 * advance the reader to the next line
	 * @throws IOException
	 */
	public void advance() throws IOException {
		currLine = nextLine;
		removeCommentsAndSpaces();
	}
	
	/**
	 * removing comments and spaces from the current line
	 */
	private void removeCommentsAndSpaces() {
		if(this.currLine.contains("//")){
			//removing possible comments
			currLine = currLine.substring(0, currLine.indexOf("//"));
		}
		//removing possible empty spaces from the start and the end
		currLine = currLine.trim();
	}

	/**
	 * check what type of line is the current line
	 * @return the type of the current line according to the enom defined in 
	 * the HackAssembler.java file
	 * @see Assembler.java
	 */
	public Assembler.CommandType commandType() {
		currLine.trim(); // removing spaces
		if(currLine.isEmpty()) { 
			return  Assembler.CommandType.NO_COMMAND; //the line is empty or comment line
		}
		else if (currLine.startsWith("@")) {
			return Assembler.CommandType.A_COMMAND;
		}else if (currLine.startsWith("(")) {
			return Assembler.CommandType.L__COMMAND;
		}else {
			// Initiate dest, comp and jump fields
			this.splitC_Command();
			return Assembler.CommandType.C_COMMAND;
		}
	}


	/**
	 * assuming called iff the command is L or A
	 * @return the symbol of the 	L\A command
	 */
	public String symbol() {
		String processedLine;
		if(this.commandType() == Assembler.CommandType.L__COMMAND){
			processedLine =  currLine.substring(1, currLine.length()-1);
			processedLine.trim(); //removing possible spaces within the braces
		}else {
			processedLine = currLine.substring(1);
		}
		return processedLine;
	}
	
	/**
	 * assuming the current command is c-command
	 * @return the dest part of the command
	 */
	public String dest() {
		return this.dest;
	}
	
	/**
	 * assuming the current command is c-command
	 * @return the comp part of the command
	 */
	public String comp() {
		return this.comp;
	}
	
	/**
	 * assuming the current command is c-command
	 * @return the jump part of the command
	 */
	public String jump() {
		return this.jump;
	}
	
	/**
	 * split a c-command to it's parts and puttong each one of them in it's proper inner field
	 */
	private void splitC_Command(){
		// need implementation
		this.comp=null;
		this.dest=null;
		this.jump=null;
		String[] restAndJump = currLine.split(";");
		String[] destAndRest = currLine.split("=");
		String[] destAndComp = restAndJump[0].split("=");
		
		//finding comp
		int compPlace = 0; // the index of comp in restAndJump
		if(destAndComp.length == 2)
		{
			// there is a dest in destAndComp
			compPlace = 1;
		}
		this.comp = destAndComp[compPlace].trim();
		
		//finding dest
		if(destAndRest.length==2){
			//there is a dest - find it
			this.dest = destAndRest[0].trim();
		}
		
		//finding jump
		if(restAndJump.length == 2){
			//there is a jump - find it
			this.jump = restAndJump[1].trim();
		}
	}
	
}
