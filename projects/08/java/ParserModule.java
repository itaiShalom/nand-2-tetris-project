import java.io.BufferedReader;
import java.io.IOException;


public class ParserModule {

	private String currLine,nextLine;
	private BufferedReader buffer;
	private final String[] arithmeticCommands = {"add","sub","neg","eq","gt","lt","and","or","not"};
	private final String sPush = "push";
	private final String sPop = "pop";
	private final String sLabel = "label";
	private final String sGoto = "goto";
	private final String sIfGoto = "if-goto";
	private final String sFunction = "function";
	private final String sReturn = "return";
	private final String sCall = "call";

	private String[] s_arrArgs;//= new String[2];
	private final int ARG_ZERO =0;
	private final int ARG_ONE =1;
	private final int ARG_TWO =2;
	/**
	 * @param args
	 */
	public ParserModule(BufferedReader asmFileBuffer) throws IOException{
		// TODO Auto-generated constructor stub
		currLine = null;
		buffer = asmFileBuffer;
		nextLine=buffer.readLine();
//		args= new String[NUM_OF_ARGS];
	}

	public boolean hasMoreCommands() throws IOException {

		return ((nextLine=buffer.readLine()) !=null);
	}
	public void advance() throws IOException {
		currLine = nextLine;
		removeCommentsAndSpaces();
	}
	private void removeCommentsAndSpaces() {

		if(this.currLine.contains("//")){
			//removing possible comments
			currLine = currLine.substring(0, currLine.indexOf("//"));
		}
		//removing possible empty spaces from the start and the end
		currLine = currLine.trim();
	}

	public MainModule.CommandType commandType() {
		currLine.trim(); // removing spaces
		setArgs(currLine);
		if(currLine.isEmpty())
		{ 
			
			return  MainModule.CommandType.NO_COMMAND; //the line is empty or comment line
		}
		else if (isArithmetic(currLine.toLowerCase()))
		{
			return MainModule.CommandType.C_ARITHMETIC;
		}
		else if(currLine.toLowerCase().startsWith(sPop))
		{
			return MainModule.CommandType.C_POP;
		}
		else if(currLine.toLowerCase().startsWith(sPush))
		{
			return MainModule.CommandType.C_PUSH;
		}
		else if(currLine.toLowerCase().startsWith(sLabel))
		{
			return MainModule.CommandType.C_LABEL;
		}
		else if(currLine.toLowerCase().startsWith(sGoto))
		{
			return MainModule.CommandType.C_GOTO;
		}
		else if(currLine.toLowerCase().startsWith(sIfGoto))
		{
			return MainModule.CommandType.C_IF;
		}
		else if(currLine.toLowerCase().startsWith(sFunction))
		{
			return MainModule.CommandType.C_FUNCTION;
		}
		else if(currLine.toLowerCase().startsWith(sReturn))
		{
			return MainModule.CommandType.C_RETURN;
		}
		else // if(currLine.toLowerCase().startsWith(sCall)) //Assumes this is the case
		{
			return MainModule.CommandType.C_CALL;
		}
	}
	
	private void setArgs(String sLine)
	{
		s_arrArgs = sLine.split(" ");	
	}
	
	private boolean isArithmetic(String command)
	{
		for(int i=0;i<arithmeticCommands.length;i++)
		{
			if(arithmeticCommands[i].equals(command))
			{
				return true;
			}
		}
		return false;
	}

	public String arg1(){
		if (s_arrArgs.length==ARG_ONE)
		{
			return s_arrArgs[ARG_ZERO];
		}
		return s_arrArgs[ARG_ONE];
	}
	
	public int arg2(){
		return Integer.parseInt(s_arrArgs[ARG_TWO]);
	}
}
