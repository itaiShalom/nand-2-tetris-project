import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

//import MainModule.CommandType;


public class CodeWriter {

	private PrintWriter _sOutputPrinter;
	private String currFile, currFunction;
	private int eqCount, gtCount, ltCount, returnCount;
	private String SYS_INIT = "Sys.init";
	
	public CodeWriter(PrintWriter sOutputPrinter) throws IOException{
		_sOutputPrinter = sOutputPrinter;
		eqCount = 0;
		gtCount = 0;
		ltCount = 0;
		returnCount = 0;
		currFunction="";
		writeInit();
	}
	
	/**
	 * initializing RAM[SP] = 256. remove to init
	 */
	private void writeInit()
	{
		this._sOutputPrinter.println("@256");
		this._sOutputPrinter.println("D=A");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=D");
		writeCall(SYS_INIT, 0);
	}
	
	public void writeLabel(String label)
	{
		this._sOutputPrinter.println("("+currFunction+"$"+label+")");
	}
	
	public void writeGoTo(String label)
	{
		this._sOutputPrinter.println("@"+currFunction+"$"+label);
		this._sOutputPrinter.println("0;JMP");
	}
	
	public void writeIf(String label)
	{
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("AM=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@"+currFunction+"$"+label);
		this._sOutputPrinter.println("D;JNE");
	}
	
	public void writeArithmetic(String command)
	{
		if(command.equals("add")){
			writeAddCommand();
			//_sOutputPrinter.println("@"+(sp-1));
			//_sOutputPrinter.println("D=M");
			//sp--;
			//_sOutputPrinter.println("@"+sp);
			//_sOutputPrinter.println("D=D+M");
			//_sOutputPrinter.println("M=D");
			//_sOutputPrinter.println("@0");
			//_sOutputPrinter.println("M=M+1");
		}else if(command.equals("sub")){
			writeSubCommand();
		}else if (command.equals("neg")) {
			writeNegCommand();
		}else if (command.equals("eq")) {
			writeEqCommand();
		}else if (command.equals("gt")) {
			writeGtCommand();
		}else if (command.equals("lt")) {
			writeLtCommand();
		}else if (command.equals("and")) {
			writeAndCommand();
		}else if (command.equals("or")) {
			writeOrCommand();
		}else {
			writeNotCommand();
		}
	}
	
	public void setFileName(String fileName)
	{
	//	if (fileName.contains(File.separator.toString()))
				{
	//		fileName = fileName.replace(File.separator.toString(), "");
				}
		this.currFile = fileName;
	}
	
	public void WritePushPop(MainModule.CommandType command, String segment, int index)
	{
		switch (segment) {
		case "constant":
			writeConstantPush(index);
			//the "writeConstantPush" does all the work
			return;
		case "argument":
			writeArgument(index);
			break;
		case "local":
			writeLocal(index);
			break;
		case "static":
			if(command == MainModule.CommandType.C_PUSH){
				writePushStatic(index);
			}else {
				writeStaticPop(index);
			}
			// the write static functions do all the work in this case
			return;
		case "this":
			writeThis(index);
			break;
		case "that":
			writeThat(index);
			break;
		case "pointer":
			writePointer(index);
			break;
		case "temp":
			writeTemp(index);
			break;
		default:
			break;
		}
		this._sOutputPrinter.println("@"+index);
		this._sOutputPrinter.println("D=D+A");
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("M=D");
		switch (command) {
		case C_PUSH:
			writePush();
			break;
		case C_POP:
			writePop();
		default:
			break;
		}

	}
	
	public void close()
	{
		_sOutputPrinter.close();	
	}
	public void writeCall(String functionName, int numArgs) {
		this.returnCount++;
		pushReturnAddress();
		pushLCL();
		pushARG();
		pushTHIS();
		pushTHAT();
		this._sOutputPrinter.println("@"+numArgs);
		this._sOutputPrinter.println("D=A");
		this._sOutputPrinter.println("@5");
		this._sOutputPrinter.println("D=D+A");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("D=M-D");//D=SP-(numArgs)
		this._sOutputPrinter.println("@ARG");
		this._sOutputPrinter.println("M=D");//ARG=D
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@LCL");
		this._sOutputPrinter.println("M=D");//LCL=SP
		this._sOutputPrinter.println("@"+functionName);
		this._sOutputPrinter.println("0;JMP");//goto functionName
		this._sOutputPrinter.println("(RETURN"+this.returnCount+")");//(return-address)
	}
	private void pushTHAT() {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@THAT");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");
		
	}
	private void pushTHIS() 
	{
		this._sOutputPrinter.println("@THIS");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");
		
	}
	
	private void pushARG() 
	{
		this._sOutputPrinter.println("@ARG");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");
		
	}
	
	private void pushLCL() 
	{
		this._sOutputPrinter.println("@LCL");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");		
	}
	private void pushReturnAddress() {
		this._sOutputPrinter.println("@RETURN"+this.returnCount);
		this._sOutputPrinter.println("D=A");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");		
	}
	public void writeReturn() {
		//R13=LCL. It will save the "FRAME" of the function
		this._sOutputPrinter.println("@LCL");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("M=D");
		
		//R14=*(LCL-5). It will save the return address of the function
		this._sOutputPrinter.println("@5");
		this._sOutputPrinter.println("D=D-A");
		this._sOutputPrinter.println("A=D");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@R14");
		this._sOutputPrinter.println("M=D");
		
		// *(ARG)=pop()
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("AM=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@ARG");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		
		//SP=ARG+1
		this._sOutputPrinter.println("@ARG");
		this._sOutputPrinter.println("D=M+1");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=D");
		
		//R13--, THAT=*(R13)
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("AM=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@THAT");
		this._sOutputPrinter.println("M=D");
		
		//R13--, THIS=*(R13)
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("AM=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@THIS");
		this._sOutputPrinter.println("M=D");
		
		//R13--, ARG=*(R13)
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("AM=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@ARG");
		this._sOutputPrinter.println("M=D");
		
		//R13--, LCL=*(R13)
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("AM=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@LCL");
		this._sOutputPrinter.println("M=D");
		
		//goto RAM[R14]
		this._sOutputPrinter.println("@R14");
		this._sOutputPrinter.println("A=M;JMP");
	}
	public void writeFunction(String functionName, int numLocals) {
		this._sOutputPrinter.println("(" + functionName + ")");
		//maybe add tabs
		for (int i = 0; i < numLocals; i++) {
			writeConstantPush(0);
		}
		this.currFunction = functionName;
	}
	
	private void writePop() {
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("AM=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		
	}
	
	private void writePush() {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");
	}
	
	private void writeAddCommand() {
		// TODO Auto-generated method stub
		//SP = SP-1
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M-1");
		
		//get the first element
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("D=M");
		//one more step into the stack - to the second elemet
		this._sOutputPrinter.println("A=A-1");
		// adding the two elements
		this._sOutputPrinter.println("M=M+D");
		
	}private void writeSubCommand() {
		// TODO Auto-generated method stub
		//SP = SP-1
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M-1");
		
		//get the first element
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("D=M");
		//one more step into the stack - to the second elemet
		this._sOutputPrinter.println("A=A-1");
		//Subtracting the two elements
		this._sOutputPrinter.println("M=M-D");
		
	}
	private void writeNegCommand() {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=-M");
	}
	private void writeEqCommand() {
		eqCount++;
		this._sOutputPrinter.println("\t@SP");
		this._sOutputPrinter.println("\tM=M-1");
		
		//get the first element
		this._sOutputPrinter.println("\tA=M");
		this._sOutputPrinter.println("\tD=M");
		//one more step into the stack - to the second element
		this._sOutputPrinter.println("\tA=A-1");
		// D = stack[1] - stack[0]
		this._sOutputPrinter.println("\tD=M-D");
		
		// jump to EQ if D==0
		this._sOutputPrinter.println("\t@EQ" + eqCount);
		this._sOutputPrinter.println("\tD;JEQ");
		
		// D!=0
		this._sOutputPrinter.println("\t@SP");
		this._sOutputPrinter.println("\tA=M-1");
		this._sOutputPrinter.println("\tM=0");
		
		// skip the option of  D==0
		this._sOutputPrinter.println("\t@ENDEQ" + eqCount);
		this._sOutputPrinter.println("\t0;JMP");
		
		// D==0
		this._sOutputPrinter.println("(EQ" + eqCount + ")");
		this._sOutputPrinter.println("\t@SP");
		this._sOutputPrinter.println("\tA=M-1");
		this._sOutputPrinter.println("\tM=-1");
		
		//END
		this._sOutputPrinter.println("(ENDEQ" + eqCount + ")");
	}
	
	
	private void writeGtCommand() {
		// TODO Auto-generated method stub
		gtCount++;
		this._sOutputPrinter.println("\t@SP");
		this._sOutputPrinter.println("\tM=M-1");
		
		//get the first element
		this._sOutputPrinter.println("\tA=M");
		this._sOutputPrinter.println("\tD=M");
		//one more step into the stack - to the second element
		this._sOutputPrinter.println("\tA=A-1");
		// D = stack[1] - stack[0]
		this._sOutputPrinter.println("\tD=M-D");
		
		// jump to GT if D>0
		this._sOutputPrinter.println("\t@GT" + gtCount);
		this._sOutputPrinter.println("\tD;JGT");
		
		// D<=0
		this._sOutputPrinter.println("\t@SP");
		this._sOutputPrinter.println("\tA=M-1");
		this._sOutputPrinter.println("\tM=0");
		
		// skip the option of  D>0
		this._sOutputPrinter.println("\t@ENDGT" + gtCount);
		this._sOutputPrinter.println("\t0;JMP");
		
		// D>0
		this._sOutputPrinter.println("(GT" + gtCount + ")");
		this._sOutputPrinter.println("\t@SP");
		this._sOutputPrinter.println("\tA=M-1");
		this._sOutputPrinter.println("\tM=-1");
		
		//END
		this._sOutputPrinter.println("(ENDGT" + gtCount + ")");
		
	}
	private void writeLtCommand() {
		// TODO Auto-generated method stub
		ltCount++;
		this._sOutputPrinter.println("\t@SP");
		this._sOutputPrinter.println("\tM=M-1");
		
		//get the first element
		this._sOutputPrinter.println("\tA=M");
		this._sOutputPrinter.println("\tD=M");
		//one more step into the stack - to the second element
		this._sOutputPrinter.println("\tA=A-1");
		// D = stack[1] - stack[0]
		this._sOutputPrinter.println("\tD=M-D");
		
		// jump to LT if D<0
		this._sOutputPrinter.println("\t@LT" + ltCount);
		this._sOutputPrinter.println("\tD;JLT");
		
		// D>=0
		this._sOutputPrinter.println("\t@SP");
		this._sOutputPrinter.println("\tA=M-1");
		this._sOutputPrinter.println("\tM=0");
		
		// skip the option of  D>=0
		this._sOutputPrinter.println("\t@ENDLT" + ltCount);
		this._sOutputPrinter.println("\t0;JMP");
		
		// D>=0
		this._sOutputPrinter.println("(LT" + ltCount + ")");
		this._sOutputPrinter.println("\t@SP");
		this._sOutputPrinter.println("\tA=M-1");
		this._sOutputPrinter.println("\tM=-1");
		
		//END
		this._sOutputPrinter.println("(ENDLT" + ltCount + ")");
		
	}
	private void writeAndCommand() {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M-1");
		
		//get the first element
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("D=M");
		//one more step into the stack - to the second elemet
		this._sOutputPrinter.println("A=A-1");
		//Subtracting the two elements
		this._sOutputPrinter.println("M=D&M");
	}
	private void writeOrCommand() {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M-1");
		
		//get the first element
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("D=M");
		//one more step into the stack - to the second elemet
		this._sOutputPrinter.println("A=A-1");
		//Subtracting the two elements
		this._sOutputPrinter.println("M=D|M");
	}
	private void writeNotCommand() {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=!M");
	}



	private void writePushStatic(int index) {
		this._sOutputPrinter.println("@" + currFile + "." + index);
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");
	}

	private void writeTemp(int index) {
		this._sOutputPrinter.println("@5");
		this._sOutputPrinter.println("D=A");
		
	}
	private void writePointer(int index) {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@THIS");
		this._sOutputPrinter.println("D=A");
		
	}
	private void writeThat(int index) {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@THAT");
		this._sOutputPrinter.println("D=M");
		
	}
	private void writeThis(int index) {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@THIS");
		this._sOutputPrinter.println("D=M");
		
	}
	private void writeStaticPop(int index) {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@" + currFile + "." + index);
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M-1");
	}
	private void writeLocal(int index) {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@LCL");
		this._sOutputPrinter.println("D=M");
	}
	private void writeArgument(int index) {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@ARG");
		this._sOutputPrinter.println("D=M");
		
	}
	private void writeConstantPush(int index) {
		//System.out.println(index);
		this._sOutputPrinter.println("@"+index);
		this._sOutputPrinter.println("D=A");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");
		
	}
}
