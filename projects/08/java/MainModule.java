/**
 * Proudly Written By Itai Shalom and Yonatan Tahan
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;



public class MainModule {
	public static enum CommandType{
		C_ARITHMETIC, C_PUSH, C_POP,C_LABEL,C_GOTO,C_IF,C_FUNCTION,C_RETURN,C_CALL,NO_COMMAND
	}

	public static void main(String[] args) throws IOException

	{
		File file = new File(args[0]);

		if (file.isFile()){
			
			String newFile = file.getAbsolutePath().replace(".vm", ".asm");
			PrintWriter writer = new PrintWriter(newFile, "UTF-8");
			CodeWriter myCodeWriter = new CodeWriter(writer);
			driver(file.getAbsolutePath(),myCodeWriter);
			myCodeWriter.close();
		}else{
			
			PrintWriter writer = new PrintWriter(file.getAbsolutePath()+"\\"+file.getName()+".asm", "UTF-8");
			CodeWriter myCodeWriter = new CodeWriter(writer);
			File[] listOfFiles = file.listFiles();

			for (int i = 0; i < listOfFiles.length; i++)
			{
				if (listOfFiles[i].isFile() && listOfFiles[i].getAbsolutePath().endsWith(".vm"))
				{
					driver(listOfFiles[i].getAbsolutePath(),myCodeWriter);
				} 
			}
			myCodeWriter.close();
		}
	
	}
	
	private static void driver(String sInputFileName,CodeWriter myCodeWriter) throws FileNotFoundException, IOException
	{
		String fileName = sInputFileName.replace(".vm", "");
		
		if(fileName.contains(File.separator))
		{
			fileName = fileName.substring(fileName.lastIndexOf(File.separator)+1, fileName.length());
		}

		myCodeWriter.setFileName(fileName);
		ParserModule myParser = new ParserModule(new BufferedReader(new FileReader(sInputFileName)));
		do
		{
			myParser.advance();
			CommandType command = myParser.commandType();
			switch (command)
			{
			case C_ARITHMETIC:
				myCodeWriter.writeArithmetic(myParser.arg1());
				break;
			case C_PUSH:
				myCodeWriter.WritePushPop(command, myParser.arg1(), myParser.arg2());
				break;
			case C_POP:
				myCodeWriter.WritePushPop(command, myParser.arg1(), myParser.arg2());
				break;
			case C_LABEL:
				myCodeWriter.writeLabel(myParser.arg1());
				break;
			case C_GOTO:
				
				myCodeWriter.writeGoTo(myParser.arg1());
				break;
			case C_IF:
				myCodeWriter.writeIf(myParser.arg1());
				break;
			case C_FUNCTION:
				myCodeWriter.writeFunction(myParser.arg1(), myParser.arg2());
				break;
				
			case C_RETURN:
				myCodeWriter.writeReturn();
				break;
				
			case C_CALL:
				myCodeWriter.writeCall(myParser.arg1(), myParser.arg2());
				break;
			default:
				continue;
			}
		}while (myParser.hasMoreCommands());
	}

}

