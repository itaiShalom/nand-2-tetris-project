import java.io.IOException;
import java.io.PrintWriter;

//import MainModule.CommandType;


public class CodeWriter {

    String TEMP ="@5";
	private PrintWriter _sOutputPrinter;
	private String currFile;
	private static int eqCount, gtCount, ltCount;
	public CodeWriter(PrintWriter sOutputPrinter) throws IOException{
		_sOutputPrinter = sOutputPrinter;
		eqCount = 0;
		gtCount = 0;
		ltCount = 0;
		// initializing RAM[SP] = 256
		//this._sOutputPrinter.println("@256");
		//this._sOutputPrinter.println("D=A");
		//this._sOutputPrinter.println("@SP");
		//this._sOutputPrinter.println("M=D");
	}
	public void writeArithmetic(String command)
	{
		if(command.equals("add")){
			writeAddCommand();
		}else if(command.equals("sub")){
			writeSubCommand();
		}else if (command.equals("neg")) {
			writeNegCommand();
		}else if (command.equals("eq")) {
			writeEqCommand();
		}else if (command.equals("gt")) {
			writeGtCommand();
		}else if (command.equals("lt")) {
			writeLtCommand();
		}else if (command.equals("and")) {
			writeAndCommand();
		}else if (command.equals("or")) {
			writeOrCommand();
		}else {
			writeNotCommand();
		}
	}
	
	public void setFileName(String fileName)
	{
		this.currFile = fileName;
	}
	
	public void WritePushPop(MainModule.CommandType command, String segment, int index)
	{
		switch (segment) {
		case "constant":
			writeConstantPush(index);
			//the "writeConstantPush" does all the work
			return;
		case "argument":
            getName("ARG");
			break;
		case "local":
            getName("LCL");
			break;
		case "static":
			if(command == MainModule.CommandType.C_PUSH){
				writePushStatic(index);
			}else {
				writeStaticPop(index);
			}
			// the write static functions do all the work in this case
			return;
		case "this":
            getName("THIS");
			break;
		case "that":
            getName("THAT");
			break;
		case "pointer":
			writePointer();
			break;
		case "temp":
			writeTemp();
			break;
		default:
			break;
		}
		this._sOutputPrinter.println("@"+index);
		this._sOutputPrinter.println("D=D+A");
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("M=D");
		switch (command) {
		case C_PUSH:
			writePush();
			break;
		case C_POP:
			writePop();
		default:
			break;
		}

	}
	
	public void close()
	{
		_sOutputPrinter.close();	
	}
	
	private void writePop() {
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("AM=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		
	}
	
	private void writePush() {
		// TODO Auto-generated method stub
//		this._sOutputPrinter.println("@R13");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");
	}
	
	private void writeAddCommand() {
		getTwoLastArguments();
		// adding the two elements
		this._sOutputPrinter.println("M=M+D");
		
	}private void writeSubCommand() {
		getTwoLastArguments();
		//Subtracting the two elements
		this._sOutputPrinter.println("M=M-D");
		
	}
	private void writeNegCommand() {
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=-M");
	}
	private void writeEqCommand() {
		eqCount++;
		getTwoLastArguments();
		// D = stack[1] - stack[0]
		this._sOutputPrinter.println("D=M-D");
		
		// jump to EQ if D==0
		this._sOutputPrinter.println("@EQ" + eqCount);
		this._sOutputPrinter.println("D;JEQ");
		
		// D!=0
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=0");
		
		// skip the option of  D==0
		this._sOutputPrinter.println("@ENDEQ" + eqCount);
		this._sOutputPrinter.println("0;JMP");
		
		// D==0
		this._sOutputPrinter.println("(EQ" + eqCount + ")");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=-1");
		
		//END
		this._sOutputPrinter.println("(ENDEQ" + eqCount + ")");
	}
	
	private void getTwoLastArguments()
	{
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M-1");

		//get the first element
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("D=M");
		//one more step into the stack - to the second element
		this._sOutputPrinter.println("A=A-1");
	}


	private void writeGtCommand() {

		gtCount++;
		getTwoLastArguments();
		// D = stack[1] - stack[0]
		this._sOutputPrinter.println("D=M-D");
		
		// jump to GT if D>0
		this._sOutputPrinter.println("@GT" + gtCount);
		this._sOutputPrinter.println("D;JGT");
		
		// D<=0
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=0");
		
		// skip the option of  D>0
		this._sOutputPrinter.println("@ENDGT" + gtCount);
		this._sOutputPrinter.println("0;JMP");
		
		// D>0
		this._sOutputPrinter.println("(GT" + gtCount + ")");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=-1");
		
		//END
		this._sOutputPrinter.println("(ENDGT" + gtCount + ")");
		
	}
	private void writeLtCommand() {

		ltCount++;
		getTwoLastArguments();
		// D = stack[1] - stack[0]
		this._sOutputPrinter.println("D=M-D");
		
		// jump to LT if D<0
		this._sOutputPrinter.println("@LT" + ltCount);
		this._sOutputPrinter.println("D;JLT");
		
		// D>=0
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=0");
		
		// skip the option of  D>=0
		this._sOutputPrinter.println("@ENDLT" + ltCount);
		this._sOutputPrinter.println("0;JMP");
		
		// D>=0
		this._sOutputPrinter.println("(LT" + ltCount + ")");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=-1");
		
		//END
		this._sOutputPrinter.println("(ENDLT" + ltCount + ")");
		
	}
	private void writeAndCommand() {
		getTwoLastArguments();
		//Subtracting the two elements
		this._sOutputPrinter.println("M=D&M");
	}
	private void writeOrCommand() {
		getTwoLastArguments();
		//Subtracting the two elements
		this._sOutputPrinter.println("M=D|M");
	}
	private void writeNotCommand() {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("M=!M");
	}



	private void writePushStatic(int index) {
		this._sOutputPrinter.println("@" + currFile + "." + index);
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");
	}

	private void writeTemp() {
		this._sOutputPrinter.println(TEMP);
		this._sOutputPrinter.println("D=A");
		
	}
	private void writePointer() {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@THIS");
		this._sOutputPrinter.println("D=A");
		
	}

	private void writeStaticPop(int index) {
		// TODO Auto-generated method stub
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M-1");
		this._sOutputPrinter.println("D=M");
		this._sOutputPrinter.println("@" + currFile + "." + index);
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M-1");
	}

    private void getName(String sName)
    {
        this._sOutputPrinter.println("@"+sName);
        this._sOutputPrinter.println("D=M");
    }
	private void writeConstantPush(int index) {
		// TODO Auto-generated method stub
		//System.out.println(index);
		this._sOutputPrinter.println("@"+index);
		this._sOutputPrinter.println("D=A");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("A=M");
		this._sOutputPrinter.println("M=D");
		this._sOutputPrinter.println("@SP");
		this._sOutputPrinter.println("M=M+1");
		
	}
}

