//dividing two positive integers
//R13/R14-->R15

//dividend=r13
@13
D=M
@dividend
M=D

//divisor=r14
@14
D=M
@divisor
M=D

//R15=0
@15
M=0

//if(divisor==1) goto DIVISORIS1 (R15=R13 and goto END) 

@divisor
D=M
@1
D=D-A
@DIVISORIS1
D;JEQ

(LOOP)

    //temp = 1
    @1
    D=A
    @temp
    M=D
    
    //if(dividend < divisor) goto END
    @dividend
    D=M
    @divisor
    D=D-M
    @END
    D;JLT
    
    //counter = i = 0
    @0
    D=A
    @counter
    M=D
    @0
    D=A
    @i
    M=D

    //c=divisor
    @divisor
    D=M
    @c
    M=D

    (FIRSTLOOP)

        //if(dividend-c <= 0) goto SECONDLOOP
        @c
        D=M
        @dividend
        D=D-M
        @SECONDLOOP
        D;JGE

        //c *=2; counter++
        @c
        M=M<<
        @counter
        M = M+1
        
        @FIRSTLOOP
        0;JMP

    (SECONDLOOP)

        // if(i-counter+1 >= 0) goto MAINLOOP
        @i
        D=M//SHOULDBE -1
        @counter
        D=D-M
        @1
        D=D+A
        @MAINLOOP
        D;JGE
        
        //temp *=2
        @temp
        M=M<<

        //i++
        @i
        M = M+1

        @SECONDLOOP
        0;JMP

    (MAINLOOP)
        //c /=2
        @c
        M=M>>

        //R15 += temp
        @temp
        D=M
        @15
        M=M+D

        //dividend -= c
        @c
        D=M
        @dividend
        M=M-D

        @LOOP
        0;JMP

(DIVISORIS1)
    @dividend
    D=M
    @15
    M=D
    @END
    0;JMP

(END)
        
         


















    
