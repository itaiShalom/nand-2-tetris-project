// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input. 
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel. When no key is pressed, the
// program clears the screen, i.e. writes "white" in every pixel.

//set max to the last sel of the screen + 1 == KBD
@KBD
D=A
@max
M=D

// "Game Loop"
(INIFNITELOOP)

    // i = SCREEN
    @SCREEN
    D=A
    @i
    M=D

    // if memory[KBD] !=0 goto BLACKLOOP else goto WHITELOOP
    @KBD
    D=M
    @BLACKLOOP
    D;JNE
    @WHITELOOP
    0;JMP
    
(BLACKLOOP)
    //if(i==max) end black loop & start over
    @i
    D=M
    @max
    D=D-M
    @INIFNITELOOP
    D;JEQ

    // memory[i] = -1 (i.e black)
    @i
    A=M
    M=-1
    
    //i++
    @i
    M=M+1
    
    //repeat
    @BLACKLOOP
    0;JMP

(WHITELOOP)
    //if(i==max) end white loop & start over
    @i
    D=M
    @max
    D=D-M
    @INIFNITELOOP
    D;JEQ

    // memory[i] = 0 (i.e white)
    @i
    A=M
    M=0

    //i++
    @i
    M=M+1

    //repeat
    @WHITELOOP
    0;JMP
    
    
