// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)
	//Ensure Outout is zeri
	@2
	M=0
	//Check if first Variable is zero
	@1
	D=M
	@END
	D;JEQ
(Loop)
	@0
	D=M
	@END
	D;JEQ

	
	@2
	M=D+M

	@1
	M=M-1
	D=M
	@END
	D;JEQ
	@Loop
	0;JMP
(END)