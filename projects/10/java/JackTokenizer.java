import java.io.BufferedReader;



import java.io.IOException;

import java.io.Reader;

import java.io.StreamTokenizer;

import java.util.ArrayList;

import java.util.regex.Matcher;

import java.util.regex.Pattern;





public class JackTokenizer {



	private String sCurrentToken;

	String keyString = "class|constructor|function|method|field|static|var|int|" +

			"char|boolean|void|true|false|null|this|let|do|if|else|while|return";





	private enum typeRead{

		WORD,CHAR,NUMBER,STRING_CONST

	}

	private JackAnalyzer.Keyword enCurrentKeyword;

	typeRead enCurrentType;

	int lineNum;

	int iLastLine;

	// print the stream tokens

	boolean isFirst;

	boolean eof;

	int token;

	ArrayList<String> functionCallSeperated;

	StreamTokenizer st;



	private boolean isFunctionCall = false;

	private String[] splitedFunctionCall;

	private Pattern p;

	private boolean isNeg=false;

	private int negInt;

	private int indexOfSplitedFunctionCall;

	public JackTokenizer(BufferedReader asmFileBuffer) throws IOException

	{



		// create a new tokenizer

		Reader r = asmFileBuffer;

		st = new StreamTokenizer(r);

		st.ordinaryChar('/');

		st.ordinaryChar('-');

		st.wordChars(95, 95);

		st.slashStarComments(true);

		st.slashSlashComments(true);



		lineNum =0;

		iLastLine=0;

		// print the stream tokens

		isFirst= true;

		eof = false;

		

		 p = Pattern.compile(keyString);

		 

	}



	public boolean hasMoreTokens()

	{

		return !eof;

	}





	private void updateCurrentTokenOfFunctionCall()

	{

		if (indexOfSplitedFunctionCall<functionCallSeperated.size())

		{

			sCurrentToken = functionCallSeperated.get(indexOfSplitedFunctionCall);

			if (sCurrentToken.equals("."))

				enCurrentType = typeRead.CHAR;

			else

				enCurrentType = typeRead.WORD;

			indexOfSplitedFunctionCall++;

			if (indexOfSplitedFunctionCall==functionCallSeperated.size()){

				isFunctionCall=false;

			}

		}

	}





	public void advance() throws IOException{

		

		if(isNeg)

		{

			isNeg=false;

			

			enCurrentType = typeRead.NUMBER;

			sCurrentToken = Integer.toString(negInt);

			return;

		}

		if (isFunctionCall)

		{

			updateCurrentTokenOfFunctionCall();

			return;

		}

		if(isFirst)

		{

			token  = st.nextToken();

			isFirst =false;

			iLastLine = st.lineno()-1;

		}else{

			iLastLine = st.lineno();

			token = st.nextToken();

		}

		switch (token) {

		case StreamTokenizer.TT_EOF:

			eof = true;

			return;

			

		case StreamTokenizer.TT_EOL:

			break;

		case StreamTokenizer.TT_WORD:

			//System.out.println(st.sval);

			if (st.lineno()!=lineNum)

			{



				checkIsFunction(st.sval);

			}   

			else{

				this.advance();

				return;

			}

			break;

		case StreamTokenizer.TT_NUMBER:

			//System.out.println((int)st.nval);

			if (st.lineno()!=lineNum)

			{

				

				//Check for negative sign

				if ((int)st.nval < 0)

				{	

					isNeg=true;

					enCurrentType = typeRead.CHAR;

					sCurrentToken = "-";

					negInt = (int)st.nval*(-1);

					return;

					

				}		

				enCurrentType = typeRead.NUMBER;

				sCurrentToken = Integer.toString((int)st.nval);

			}else{

				this.advance();

				return;

			}

			break;

		default:

			//System.out.println((char)token);

			if (st.lineno()!=lineNum){

			char c = (char)token;

			if(iLastLine!=st.lineno()&&(c=='*')){//special char starts line

				lineNum= st.lineno();

				this.advance();

			}else{

				if(((char)token) == '"')	//

				{

					sCurrentToken = st.sval;

					enCurrentType = typeRead.STRING_CONST;

					return;

					

				}

				enCurrentType = typeRead.CHAR;

				sCurrentToken = ""+(char) token;;

			}

			}

			else

			{

				this.advance();

			}

			if (token == '!') {

				eof = true;

			}

		}



	}



	public JackAnalyzer.TokenType tokenType()

	{

		switch (enCurrentType)

		{

		case STRING_CONST:

			return JackAnalyzer.TokenType.STRING_CONST;

		case WORD:

			Matcher m = p.matcher(sCurrentToken);

			if (m.find())

			{

				if(!sCurrentToken.toUpperCase().equals(m.group(0).toUpperCase()))

					return JackAnalyzer.TokenType.IDENTIFIER;

				

				enCurrentKeyword = JackAnalyzer.Keyword.valueOf(m.group(0).toUpperCase());

				return JackAnalyzer.TokenType.KEYWORD;

			}

			else

				return JackAnalyzer.TokenType.IDENTIFIER;

		

		

		case CHAR:

			return JackAnalyzer.TokenType.SYMBOL;

			



		case NUMBER:

			return JackAnalyzer.TokenType.INT_CONST;

			

	}

		return JackAnalyzer.TokenType.INT_CONST;

	}
	 
	private void checkIsFunction(String str)
	{
		splitedFunctionCall = str.split("\\.");
		if(splitedFunctionCall.length==1)
		{
			sCurrentToken = str;
		}
		else
		{
			indexOfSplitedFunctionCall=1;
			functionCallSeperated = new ArrayList<String>();
			isFunctionCall = true;
			for(int i =0;i<splitedFunctionCall.length;i++)
			{
				functionCallSeperated.add(splitedFunctionCall[i]);
				if(i+1<splitedFunctionCall.length)
					functionCallSeperated.add(".");
			}
			sCurrentToken = functionCallSeperated.get(0);
			
		}
		enCurrentType = typeRead.WORD;
	}

	public String stringVal()
	{
		return sCurrentToken;
	}
	
	public JackAnalyzer.Keyword keyWord()
	{
	
		return enCurrentKeyword;
	}
	 
	public char symbol()
	{
		return sCurrentToken.charAt(0);
	}

	public String identifier()
	{
		return sCurrentToken;
	}

	public int intVal()
	{
		return Integer.parseInt(sCurrentToken);
	}
}