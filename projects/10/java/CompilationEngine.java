import java.io.IOException;

import java.io.PrintWriter;





public class CompilationEngine {





	private boolean isIfWithoutElse;

	private String indentation;

	private int indentLevel;

	private JackTokenizer input;

	private PrintWriter output;

	public CompilationEngine(JackTokenizer inputTokenizer, PrintWriter xmlOutput) {

		this.isIfWithoutElse = false;

		this.input = inputTokenizer;

		this.output = xmlOutput;

		this.indentation = "";

		this.indentLevel = 0;

	}

	

	public void compileClass() throws IOException {

		output.println(indentation + "<class>");

		//first word should be "class"

		if(input.hasMoreTokens()){

			this.input.advance();

			if(input.tokenType()== JackAnalyzer.TokenType.KEYWORD && 

					input.keyWord() == JackAnalyzer.Keyword.CLASS){

				inceaseIndentation();

				output.println(indentation + "<keyword> class </keyword>");

			}else {

				

			}

			

			// the class name

			this.input.advance();

			if(input.tokenType()== JackAnalyzer.TokenType.IDENTIFIER && 

					input.keyWord() == JackAnalyzer.Keyword.CLASS){

				output.println(indentation + "<identifier> " + input.identifier() + " </identifier>");	

			}

			// the symbol '{'

			this.input.advance();

			if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL

					&& input.symbol() == '{'){

				output.println(indentation + "<symbol> { </symbol>");

			}

			//

			if(input.hasMoreTokens()){

				input.advance();

			}

			while (input.tokenType() == JackAnalyzer.TokenType.KEYWORD 

					&& (input.keyWord() == JackAnalyzer.Keyword.STATIC || input.keyWord() == JackAnalyzer.Keyword.FIELD) ) {

				compileClassVarDec();

				

				// advancing to the methods

				if(input.hasMoreTokens()){

					input.advance();

				}

			}

			while (input.tokenType() == JackAnalyzer.TokenType.KEYWORD  &&

					(input.keyWord() == JackAnalyzer.Keyword.CONSTRUCTOR || 

					input.keyWord() == JackAnalyzer.Keyword.METHOD

					|| input.keyWord() == JackAnalyzer.Keyword.FUNCTION)) {

				CompileSubroutine();

				input.advance();//to the next method or the end of the class

			}

			//advancing to the symbol '}'

			if(input.hasMoreTokens()){

				input.advance();

			}

			if (input.tokenType() == JackAnalyzer.TokenType.SYMBOL

					&& input.symbol() == '}') {

				output.println(indentation + "<symbol> } </symbol>");

				decreaseIndentation();

				output.println(indentation + "</class>");

				

				

			}	

		}

	}

	private void compileClassVarDec() throws IOException {

		//the token is already "static" or "field"

		output.println(indentation+"<classVarDec>");

		inceaseIndentation();

		if(input.keyWord() == JackAnalyzer.Keyword.FIELD){

			output.println(indentation+"<keyword> field </keyword>");

		}else {

			output.println(indentation+"<keyword> static </keyword>");

		}

		//the type

		input.advance();

		if(input.tokenType() == JackAnalyzer.TokenType.IDENTIFIER){

			// its a class name

			output.println(indentation+"<identifier> "+input.identifier()+ "</identifier>");

		}else if (input.tokenType() == JackAnalyzer.TokenType.KEYWORD) {

			if(input.keyWord() == JackAnalyzer.Keyword.INT){

				output.println(indentation +"<keyword> int </keyword>");

			}else if (input.keyWord() == JackAnalyzer.Keyword.CHAR) {

				output.println(indentation +"<keyword> char </keyword>");

			}else {

				output.println(indentation +"<keyword> boolean </keyword>");

			}

		}

		//variables names

		input.advance();

		while (!((input.tokenType() == JackAnalyzer.TokenType.SYMBOL) && (input.symbol() == ';'))) {

			if(input.tokenType() == JackAnalyzer.TokenType.IDENTIFIER){

				output.println(indentation +"<identifier> " + input.identifier() + " </identifier>");

				input.advance();

				continue;

			}else if (input.tokenType() == JackAnalyzer.TokenType.SYMBOL 

					&& input.symbol() == ',') {

				output.println(indentation +"<symbol> , </symbol>");

				input.advance();

				continue;

			}

		}

		output.println(indentation +"<symbol> ; </symbol>");

		//indentation = oldIndentation;

		decreaseIndentation();

		output.println(indentation +"</classVarDec>");

	}
	private void CompileSubroutine() throws IOException {

		output.println(indentation +"<subroutineDec>");

		inceaseIndentation();

		output.print(indentation +"<keyword> ");

		switch (input.keyWord()) {

		case CONSTRUCTOR:

			output.print("constructor ");

			break;

		case METHOD:

			output.print("method ");

			break;

		case FUNCTION:

			output.print("function ");

			break;

		default:

			break;

		}

		output.print("</keyword>\n");

		

		//the subroutine's return type

		input.advance();

		if(input.tokenType() == JackAnalyzer.TokenType.IDENTIFIER){

			// it's non primitive type

			output.println(indentation +"<identifier> " +input.identifier() + " </identifier>");

		}else{

			switch (input.keyWord()) {

			case VOID:
				output.println(indentation +"<keyword> void </keyword>");
				break;

			case INT:
				output.println(indentation +"<keyword> int </keyword>");
				break;

			case CHAR:
				output.println(indentation +"<keyword> char </keyword>");
				break;

			case BOOLEAN:
				output.println(indentation +"<keyword> boolean </keyword>");
				break;

			default:
				break;
			}
		}
		//the subroutine's name

		input.advance();
		output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
		input.advance();//to the symbol '{'
		if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL && input.symbol() == '('){
			compileParameterList();
		}

		

		input.advance();//to the symbol '{'

		if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL && input.symbol() == '{'){
			CompileSubroutineBody();
		}
		decreaseIndentation();
		output.println(indentation +"</subroutineDec>");
	}
	private void CompileSubroutineBody() throws IOException {
		output.println(indentation +"<subroutineBody>");
		inceaseIndentation();
		output.println(indentation +"<symbol> { </symbol>");
		input.advance();
		
		// all the variable decelerations
		while (input.tokenType() == JackAnalyzer.TokenType.KEYWORD && 
				input.keyWord() == JackAnalyzer.Keyword.VAR) {
			compileVarDec();
			//move to the next deceleration/statements
			input.advance();
		}
		compileStatements();
		if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL
				&& input.symbol() == '}')
			//the end of the subroutine
			output.println(indentation +"<symbol> } </symbol>");
			decreaseIndentation();
			output.println(indentation +"</subroutineBody>");
	}

	private void compileParameterList() throws IOException {
		output.println(indentation +"<symbol> ( </symbol>");
		output.println(indentation +"<parameterList>");
		inceaseIndentation();
		do {
			input.advance();
			switch (input.tokenType()) {
			case IDENTIFIER:
				output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
				break;
			case KEYWORD:
				switch (input.keyWord()) {
				case INT:
					output.println(indentation +"<keyword> int </keyword>");
					break;
				case CHAR:
					output.println(indentation +"<keyword> char </keyword>");
					break;
				case BOOLEAN:
					output.println(indentation +"<keyword> boolean </keyword>");
					break;
				default:
					break;
				}
				break;
			case SYMBOL:
				if(input.symbol() == ','){
					output.println(indentation +"<symbol> "+input.symbol()+ " </symbol>");					
				}
				break;
			default:
				break;
			}
			
		} while (!(input.tokenType() == JackAnalyzer.TokenType.SYMBOL && input.symbol() == ')'));

		decreaseIndentation();
		output.println(indentation +"</parameterList>");
		output.println(indentation +"<symbol> ) </symbol>");
	}

	private void compileVarDec() throws IOException {

		output.println(indentation +"<varDec>");

		inceaseIndentation();

		//the token is already checked to be the keyword "var"

		output.println(indentation +"<keyword> var </keyword>");

		//the type

		input.advance();

		if (input.tokenType() == JackAnalyzer.TokenType.KEYWORD) {

			if(input.keyWord() == JackAnalyzer.Keyword.INT){

				output.println(indentation +"<keyword> int </keyword>");

			}else if (input.keyWord() == JackAnalyzer.Keyword.CHAR) {

				output.println(indentation +"<keyword> char </keyword>");

			}else if (input.keyWord() == JackAnalyzer.Keyword.BOOLEAN){

				output.println(indentation +"<keyword> boolean </keyword>");

			}

		}

		else if(input.tokenType() == JackAnalyzer.TokenType.IDENTIFIER){

			// its a class name

			

			output.println(indentation+"<identifier> "+input.identifier()+ " </identifier>");

			

		}

		

		//variables names

		input.advance();

		while (!((input.tokenType() == JackAnalyzer.TokenType.SYMBOL) && (input.symbol() == ';'))) {

			if(input.tokenType() == JackAnalyzer.TokenType.IDENTIFIER){

				output.println(indentation +"<identifier> " + input.identifier() + " </identifier>");

				input.advance();

				continue;

			}else if (input.tokenType() == JackAnalyzer.TokenType.SYMBOL 

					&& input.symbol() == ',') {

				output.println(indentation +"<symbol> , </symbol>");

				input.advance();

				continue;

			}

		}

		

		output.println(indentation +"<symbol> ; </symbol>");

		decreaseIndentation();

		output.println(indentation +"</varDec>");

		

	}

	private void compileStatements() throws IOException {

		output.println(indentation +"<statements>");

		inceaseIndentation();

		while (!(input.tokenType() == JackAnalyzer.TokenType.SYMBOL

				&& input.symbol() == '}')) {

			if(input.tokenType() == JackAnalyzer.TokenType.KEYWORD){

				switch (input.keyWord()) {

				case LET:

					compileLet();

					break;

				case IF:

					compileIf();

					break;

				case WHILE:

					compileWhile();

					break;

				case DO:

					compileDo();

					break;

				case RETURN:

					compileReturn();

					break;



				default:

					break;

				}

			}

			// if the compileIf read the next word and it was'nt "else", skip the next read:

			if(!isIfWithoutElse){

				input.advance();

			}

			isIfWithoutElse = false;

			

		}

		decreaseIndentation();

		output.println(indentation +"</statements>");

	}

	private void compileDo() throws IOException {

		output.println(indentation +"<doStatement>");

		inceaseIndentation();

		//the token is already the keyword "do"

		output.println(indentation +"<keyword> do </keyword>");

		input.advance();

		if(input.tokenType() == JackAnalyzer.TokenType.IDENTIFIER){

			output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");

			input.advance();// to a symbol - '.' or '('

			if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL){

				switch (input.symbol()) {

				case '(':

					output.println(indentation +"<symbol> ( </symbol>");

					CompileExpressionList();

					break;

				case '.': // it's method/function of other class

					output.println(indentation +"<symbol> . </symbol>");

					input.advance();

					if(input.tokenType() == JackAnalyzer.TokenType.IDENTIFIER){

						if(input.identifier().equals("printString")){

						}

						output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");

						input.advance(); // to the symbol '('

						if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL &&

								input.symbol() == '('){

							output.println(indentation +"<symbol> ( </symbol>");

							CompileExpressionList();

						}

					}
					break;

				default:

					break;

				}

			}

			if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL &&

								input.symbol() == ')'){

				output.println(indentation +"<symbol> ) </symbol>");

				input.advance(); // to the symbol ';'

				if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL &&

								input.symbol() == ';'){

					output.println(indentation +"<symbol> ; </symbol>");

					decreaseIndentation();

					output.println(indentation +"</doStatement>");

				}

			}



		}

		

	}

	private void compileLet() throws IOException {

		this.output.println(indentation + "<letStatement>");

		inceaseIndentation();

		this.output.println(indentation +"<keyword> let </keyword>");

		this.input.advance();

		if(input.tokenType() ==  JackAnalyzer.TokenType.IDENTIFIER){

			output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");

			input.advance();

		}

		if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL){

			if (input.symbol() == '[') {

				output.println(indentation +"<symbol> [ </symbol>");

				input.advance();

				CompileExpression();

				 // to the ']' symbol

				if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && input.symbol() == ']'){

					output.println(indentation +"<symbol> ] </symbol>");

					input.advance(); // to the '='

				}

			}

			if (input.symbol() == '=') {

				output.println(indentation +"<symbol> = </symbol>");

				input.advance(); // to the expression

				CompileExpression();

			}

		}

		//end of the let statement

		if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && input.symbol() == ';'){

			output.println(indentation +"<symbol> ; </symbol>");

			decreaseIndentation();

			output.println(indentation +"</letStatement>");

		}

	}

	private void compileWhile() throws IOException {

		output.println(indentation +"<whileStatement>");

		inceaseIndentation();

		// the token is already the keyword "while"

		output.println(indentation +"<keyword> while </keyword>");

		input.advance(); // to the symbol '('

		if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && input.symbol() == '('){

			output.println(indentation +"<symbol> ( </symbol>");

			input.advance(); // to the expression

			CompileExpression();

			if (input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && input.symbol() == ')') {

				output.println(indentation +"<symbol> ) </symbol>");

				input.advance(); //to the symbol '{'

				if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && input.symbol() == '{'){

					output.println(indentation +"<symbol> { </symbol>");

					input.advance(); // to the statements

					compileStatements();

					if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && input.symbol() == '}'){

						output.println(indentation +"<symbol> } </symbol>");

					}

				}

				

			}

		}

		decreaseIndentation();

		output.println(indentation +"</whileStatement>");

		

		

	}

	private void compileReturn() throws IOException {

		output.println(indentation +"<returnStatement>");

		inceaseIndentation();

		// the token is already the keyword "return"

		output.println(indentation +"<keyword> return </keyword>");

		input.advance(); //to the expression or the symbol ';'

		if(!(input.tokenType() == JackAnalyzer.TokenType.SYMBOL && input.symbol() == ';')){

			CompileExpression();

		}

		output.println(indentation +"<symbol> ; </symbol>");

		decreaseIndentation();

		output.println(indentation +"</returnStatement>");

	}

	private void compileIf() throws IOException {

		output.println(indentation +"<ifStatement>");

		inceaseIndentation();

		output.println(indentation +"<keyword> if </keyword>");

		input.advance(); // to the symbol '('

		output.println(indentation +"<symbol> ( </symbol>");

		input.advance(); // to the expression

		CompileExpression();

		if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && input.symbol() == ')'){

			output.println(indentation +"<symbol> ) </symbol>");

			input.advance(); //to the symbol '{'

		}

		if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && input.symbol() == '{'){

			output.println(indentation +"<symbol> { </symbol>");

			input.advance(); // to the statements

			compileStatements();

		}

		if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && input.symbol() == '}'){

			output.println(indentation +"<symbol> } </symbol>");

			input.advance();

			if(!(input.tokenType() ==  JackAnalyzer.TokenType.KEYWORD &&

					input.keyWord() == JackAnalyzer.Keyword.ELSE)){

				// we read one token to far since this "if" does'nt have "else" part 

				isIfWithoutElse = true;

				decreaseIndentation();

				output.println(indentation +"</ifStatement>");

				return;

			}else{

				//there is an "else" part

				output.println(indentation +"<keyword> else </keyword>");

				input.advance(); // to the symbol '{'

				if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL &&

						input.symbol() == '{'){

					output.println(indentation +"<symbol> { </symbol>");

					input.advance(); // to the statements

					compileStatements();

				}

				if(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && 

						input.symbol() == '}'){

					output.println(indentation +"<symbol> } </symbol>");

					decreaseIndentation();

					output.println(indentation +"</ifStatement>");

				}				

			}

		}

	}

	

	private void CompileExpression() throws IOException {
		output.println(indentation +"<expression>");
		inceaseIndentation();
		CompileTerm();
		while(isOp()){
			output.println(indentation +"<symbol> " +symbolCheck(input.symbol()) +" </symbol>");
			input.advance(); //to the next term
			CompileTerm();
		}
		decreaseIndentation();
		output.println(indentation +"</expression>");
	}

	private boolean isOp() {

		if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL){

			char s = input.symbol();

			if((s == '+')

				||(s == '-')

				||(s == '*')

				||(s == '/')

				||(s == '&')

				||(s == '|')

				||(s == '<')

				||(s == '>')

				||(s == '=')){

				return true;

			}

		}

		return false;

	}



	private void CompileTerm() throws IOException {

		output.println(indentation +"<term>");

		inceaseIndentation();

		if(input.tokenType() == JackAnalyzer.TokenType.INT_CONST){

			output.println(indentation +"<integerConstant> "+input.intVal()+" </integerConstant>");

			input.advance();

		}else if (input.tokenType() == JackAnalyzer.TokenType.STRING_CONST) {

			output.println(indentation +"<stringConstant> "+stringCheck(input.stringVal())+" </stringConstant>");

			input.advance();

		}else if (input.tokenType() == JackAnalyzer.TokenType.KEYWORD) {

			output.println(indentation +"<keyword> "+constantKeyword()+" </keyword>");

			input.advance();

		}else if (input.tokenType() == JackAnalyzer.TokenType.SYMBOL) {

			output.println(indentation +"<symbol> "+symbolCheck(input.symbol())+" </symbol>");

			if(input.symbol() == '-' || input.symbol() == '~'){

				input.advance(); // to the term after the unaryOp

				CompileTerm();

			}else if (input.symbol() == '(') {

				input.advance(); // to the expression

				CompileExpression();

				if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL

						&& input.symbol() == ')'){

					output.println(indentation +"<symbol> ) </symbol>");

					input.advance();

				}

			}

		}else if (input.tokenType() == JackAnalyzer.TokenType.IDENTIFIER) {
			output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
			input.advance();
			if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL ){
				if(input.symbol() == '['){
					output.println(indentation +"<symbol> "+symbolCheck(input.symbol())+" </symbol>");
					input.advance();
					CompileExpression();
					if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL
							&& input.symbol() == ']'){
						output.println(indentation +"<symbol> ] </symbol>");
						input.advance();
					}
				}else if (input.symbol() == '(') {
					output.println(indentation +"<symbol> "+symbolCheck(input.symbol())+" </symbol>");
					CompileExpressionList();
					if (input.tokenType() == JackAnalyzer.TokenType.SYMBOL
						&& input.symbol() == ')') {
						output.println(indentation +"<symbol> ) </symbol>");
						input.advance();
					}
					
				}else if (input.symbol() == '.') {
					output.println(indentation +"<symbol> "+symbolCheck(input.symbol())+" </symbol>");
					input.advance();
					if(input.tokenType() == JackAnalyzer.TokenType.IDENTIFIER){
						output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
						input.advance(); // to the symbol '('
						if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL &&
								input.symbol() == '('){
							output.println(indentation +"<symbol> ( </symbol>");
							CompileExpressionList();
							if(input.tokenType() == JackAnalyzer.TokenType.SYMBOL &&
									input.symbol() == ')'){
								output.println(indentation +"<symbol> ) </symbol>");
								input.advance();
							}
						}
					}	
				}else if (input.symbol() == '/') {
				}
			}
		}
		decreaseIndentation();
		output.println(indentation +"</term>");

		

	}



	/*
	 * check if the current keyword is one of the following:
	 * true,false,null or this.
	 * the function asuume the current token is kewwored
	 *
     */

	private String constantKeyword() {

		switch (input.keyWord()) {

		case TRUE:

			return "true";
		case FALSE:
			return "false";
		case NULL:
			return "null";
		case THIS:
			return "this";
			
		default:

			break;
		}
		return "WTF?"; // for debug
	}



	private void CompileExpressionList() throws IOException {

		output.println(indentation +"<expressionList>");

		inceaseIndentation();

		input.advance();

		if((input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && 

				input.symbol() == ')')){

			decreaseIndentation();

			output.println(indentation +"</expressionList>");

			return;

		}else{

			do {

				CompileExpression();

				if (input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && 

						input.symbol() == ',') {

					output.println(indentation +"<symbol> , </symbol>");

					input.advance(); // to the next expression

				}

				

			} while (!(input.tokenType() ==  JackAnalyzer.TokenType.SYMBOL && 

					input.symbol() == ')'));

			decreaseIndentation();

			output.println(indentation +"</expressionList>");

			return;

		}

	}

	/*

	 * handle special characters in the xml format

	 */

	private String symbolCheck(char symbol){

		switch (symbol) {

		case '<':

			return "&lt;";

		case '>':

			return "&gt;";

		case '"':

			return "&quot;";

		case '&':

			return "&amp;";



		default:

			return Character.toString(symbol);

		}

	}

	/*
	 * handle constant strings in the jack file (make sure that escape characters stay the same and not interpeted)
	 */
	private String stringCheck(String stringConst){
		String result = stringConst;
		while(containsEscapeChar(result)){
			result = replceEscapeChars(result);
		}
		result = result.replaceAll("<", "&lt;");
		result = result.replaceAll(">", "&gt;");
		result = result.replaceAll("\"", "&quot;");
		result = result.replaceAll("&", "&amp;");
		return result;
	}
	private String replceEscapeChars(String result) {
		result = result.replace("\t", "\\t");
		result = result.replace("\n", "\\n");
		result = result.replace("\r", "\\r");
		result = result.replace("\f", "\\f");
		result = result.replace("\'", "\\'");
		return result;
	}

	private boolean containsEscapeChar(String result) {
		if(result.contains("\t") || result.contains("\n")|| result.contains("\r")
				||result.contains("\f")
				||result.contains("\'") 
				|| result.contains("\"")){
				// result.contains("\\")){
			return true;
		}
		return false;
	}


	private void inceaseIndentation(){
		indentLevel++;
		indentation ="";
		for (int i = 0; i < indentLevel; i++) {
			indentation += "  ";
		}
	}
	private void decreaseIndentation(){
		indentLevel--;
		indentation ="";
		for (int i = 0; i < indentLevel; i++) {
			indentation += "  ";
		}
	}

}