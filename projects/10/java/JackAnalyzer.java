import java.io.BufferedReader;

import java.io.File;

import java.io.FileReader;

import java.io.IOException;

import java.io.PrintWriter;



public class JackAnalyzer {



	public enum TokenType{

		KEYWORD, SYMBOL, IDENTIFIER, INT_CONST, STRING_CONST

	}

	public enum Keyword{

		CLASS, METHOD, FUNCTION, CONSTRUCTOR, INT, BOOLEAN, CHAR, VOID,

		VAR, STATIC, FIELD, LET, DO, IF, ELSE, WHILE, RETURN, TRUE, FALSE,

		NULL, THIS

	}



	public static void main(String[] args) throws IOException 

	{

		File file = new File(args[0]);

		if(file.isFile()){

			// assuming valid input

			driver(file);

		}else{

			// again, assuming valid input

			File[] filesList = file.listFiles();

			for (int i = 0; i < filesList.length; i++) {

				if(filesList[i].isFile() && filesList[i].getAbsolutePath().endsWith(".jack")){

					driver(filesList[i]);

				}

			}

		}

	}



	private static void driver(File file) throws IOException{



		// TODO Auto-generated method stub

		BufferedReader bf = new BufferedReader(new FileReader(file));

		JackTokenizer tokenizer = new JackTokenizer(bf);

		



		String xmlFile = file.getAbsolutePath().replace(".jack", ".xml");



		PrintWriter writer = new PrintWriter(xmlFile, "UTF-8");

		//System.out.println("compiling file" + file.getName());

		CompilationEngine engine = new CompilationEngine(tokenizer, writer);



		engine.compileClass();

		bf.close();

		writer.flush();

		writer.close();



	}

	







}







	



 

