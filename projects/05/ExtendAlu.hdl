/**
* The input of the extends ALU is instruction[9] and x[16],y[16].
* the output is define as follows:
* If instruction[7..8] equals 1 the the output is exactly as the ALU.
* Where instruction[5]=zx,instruction[4]=nx,...,instruction[0]=no.
* If instruction[7] equals 0 the output will be x*y and disregard the rest 
* of the instruction.
*
* If instruction[8] equals 0 the output will be shift.
* Then, if instruction[4] equals 0 it will return shift of y otherwise shift 
* of x, moreover if instruction[5] equals 0 it will return shift right 
* otherwise shift left.
**/
CHIP ExtendAlu{
     IN x[16],y[16],instruction[9];
     OUT out[16],zr,ng;
     
     PARTS:
     ALU(x=x, y=y, zx=instruction[5], nx=instruction[4], zy=instruction[3],ny=instruction[2],f=instruction[1],no=instruction[0], out= aluOut, zr=aluZr, ng=aluNg);
     Mul(a=x, b=y, out=xTimesy);
     ShiftRight(in=x, out=xsr);
     ShiftRight(in=y, out=ysr);
     ShiftLeft(in=x, out=xsl);
     ShiftLeft(in=y, out=ysl);
     //CHOOSING SHIFT
     Mux4Way16(a=ysr, b=xsr, c=ysl, d=xsl, sel=instruction[4..5], out= shiftOut);
	 
     Mux4Way16(a=xTimesy, b=shiftOut, c=xTimesy, d=aluOut, sel=instruction[7..8], out= finalOut, out[15]=msb, out[0..7]=firstHalf, out[8..15]=secondHalf);

     
     //checking if the output is 0 by "Noring" it's bits
     Or8Way(in=firstHalf, out=firstHalfOr);
     Or8Way(in=secondHalf, out=secondHalfOr);
     Or(a=firstHalfOr, b=secondHalfOr, out=finalOutOr);
     Not(in=finalOutOr, out=zr);
    
     //checking if the output is negative
     Or(a=msb, b=false, out=ng);

     //finally, assinging finalOut to out
     Or16(a=finalOut, b=false, out=out);
}
