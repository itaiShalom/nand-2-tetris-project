import java.io.BufferedReader;

import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class JackTokenizer {

	private String sCurrentToken;
	String keyString = "class|constructor|function|method|field|static|var|int|" +
			"char|boolean|void|true|false|null|this|let|do|if|else|while|return";
	/*
	public enum KeyWord {
	    CLASS, METHOD, FUNCTION, CONSTRUCTOR, INT,
	    BOOLEAN, CHAR, VOID, VAR, STATIC, FIELD,
	    LET, DO, IF, ELSE, WHILE, RETURN, TRUE,
	    FALSE, NULL, THIS
	}
	public enum TokenType {
	    KEYWORD, SYMBOL, IDENTIFIER, INT_CONST,
	    STRING_CONST
	}
	 */

	private enum typeRead{
		WORD,CHAR,NUMBER,STRING_CONST
	}
	private JackCompiler.Keyword enCurrentKeyword;
	typeRead enCurrentType;
	int lineNum;
	int iLastLine;
	// print the stream tokens
	boolean isFirst;
	boolean eof;
	int token;
	ArrayList<String> functionCallSeperated;
	StreamTokenizer st;
//	private BufferedReader buffer;
//	private String currLine;
	private boolean isFunctionCall = false;
	private String[] splitedFunctionCall;
	private Pattern p;
	private boolean isNeg=false;
	private int negInt;
	private int indexOfSplitedFunctionCall;
	public JackTokenizer(BufferedReader asmFileBuffer) throws IOException
	{
//		buffer = asmFileBuffer;
	//	nextLine=buffer.readLine();
//		currLine = null;

		//String sInputFileName = "C:\\Users\\Itai\\Documents\\mynand\\projects\\10\\Square\\Main.jack";

		// create a new tokenizer
		Reader r = asmFileBuffer;//new BufferedReader(new FileReader(sInputFileName));
		st = new StreamTokenizer(r);
		st.ordinaryChar('/');
		st.slashStarComments(true);
		st.slashSlashComments(true);
		
	//	 st.quoteChar('\"');
		lineNum =0;
		iLastLine=0;
		// print the stream tokens
		isFirst= true;
		eof = false;
		
		 p = Pattern.compile(keyString);
		 
	}

	public boolean hasMoreTokens()
	{
		return !eof;
	}


	private void updateCurrentTokenOfFunctionCall()
	{
		if (indexOfSplitedFunctionCall<functionCallSeperated.size())
		{
			sCurrentToken = functionCallSeperated.get(indexOfSplitedFunctionCall);
			if (sCurrentToken.equals("."))
				enCurrentType = typeRead.CHAR;
			else
				enCurrentType = typeRead.WORD;
			indexOfSplitedFunctionCall++;
			if (indexOfSplitedFunctionCall==functionCallSeperated.size()){
				isFunctionCall=false;
			}
		}
	}


	public void advance() throws IOException{
		if(isNeg)
		{
			isNeg=false;
			
			enCurrentType = typeRead.NUMBER;
			sCurrentToken = Integer.toString(negInt);
			return;
		}
		if (isFunctionCall)
		{
			updateCurrentTokenOfFunctionCall();
			return;
		}
		if(isFirst)
		{
			token  = st.nextToken();
			isFirst =false;
			iLastLine = st.lineno()-1;
		}else{
			iLastLine = st.lineno();
			token = st.nextToken();
		}
		//	 System.out.println(st.lineno());
		switch (token) {
		case StreamTokenizer.TT_EOF:
		//	System.out.println("End of File encountered.");
			eof = true;
			return;
			
		case StreamTokenizer.TT_EOL:
		//	System.out.println("End of Line encountered.");
			break;
		case StreamTokenizer.TT_WORD:
			
			if (st.lineno()!=lineNum)
			{ //continue;
			//	  System.out.println("Word: " + st.sval);

				checkIsFunction(st.sval);
			}   
			else{
				this.advance();
				return;
			}
			break;
		case StreamTokenizer.TT_NUMBER:
		//	 System.out.println("Word: " + st.nval);
			if (st.lineno()!=lineNum)
			{
				
				//Check for negative sign
				if ((int)st.nval < 0)
				{	
					isNeg=true;
					enCurrentType = typeRead.CHAR;
					sCurrentToken = "-";
					negInt = (int)st.nval*(-1);
					return;
					
				}		
				enCurrentType = typeRead.NUMBER;
				sCurrentToken = Integer.toString((int)st.nval);
				//System.out.println("Number: " + st.nval);
			}else{
				this.advance();
				return;
			}
			break;
		default:
		//	 System.out.println("char: " +(char)token);

			if (st.lineno()!=lineNum){
			char c = (char)token;
			if(iLastLine!=st.lineno()&&(c=='*')){//special char starts line
				lineNum= st.lineno();
				this.advance();
			}else{
				if(((char)token) == '"')	//
				{
					//parseString(st.lineno(),st.sval);
					sCurrentToken = st.sval;
					enCurrentType = typeRead.STRING_CONST;
					return;
					
				}
			//	System.out.println("char: "+(char) token );
		//		char newChar= (char) token;
				enCurrentType = typeRead.CHAR;
				sCurrentToken = ""+(char) token;
	//			sCurrentToken = newChar.toString;
			}
			}
			else
			{
				this.advance();
			}
			if (token == '!') {
				eof = true;
			}
		}

	}
	/*
	private void parseString(int lineNumber,String str) throws IOException
	{
	//	for(int i=0;i<lineNumber;i++){
		System.out.println(str);
			currLine = "\""+buffer.readLine();
		//}
		String pat ="\"[^\"]*\"";
		Pattern stringConst = Pattern.compile(pat);
		Matcher mm = stringConst.matcher(currLine);
		if(mm.find())
			sCurrentToken =mm.group(0);
		String leftOver = sCurrentToken.replace(sCurrentToken, "");
		sCurrentToken = sCurrentToken.substring(1, sCurrentToken.length()-1);
		
		enCurrentType = typeRead.STRING_CONST;
		return;
	}
	*/
	public JackCompiler.TokenType tokenType()
	{
		switch (enCurrentType)
		{
		case STRING_CONST:
			return JackCompiler.TokenType.STRING_CONST;
		case WORD:
			Matcher m = p.matcher(sCurrentToken);
			if (m.find())
			{
				if(!sCurrentToken.toUpperCase().equals(m.group(0).toUpperCase()))
					return JackCompiler.TokenType.IDENTIFIER;
				
				enCurrentKeyword = JackCompiler.Keyword.valueOf(m.group(0).toUpperCase());
				return JackCompiler.TokenType.KEYWORD;
			}
			else
				return JackCompiler.TokenType.IDENTIFIER;
		
		
		case CHAR:
			return JackCompiler.TokenType.SYMBOL;
			

		case NUMBER:
			return JackCompiler.TokenType.INT_CONST;
			
	}
		return JackCompiler.TokenType.INT_CONST;
	}
	 
	private void checkIsFunction(String str)
	{
		splitedFunctionCall = str.split("\\.");
		if(splitedFunctionCall.length==1)
		{
		//	System.out.println("Word: "+str);	
			sCurrentToken = str;
		}
		else
		{
			indexOfSplitedFunctionCall=1;
			functionCallSeperated = new ArrayList<String>();
			isFunctionCall = true;
			for(int i =0;i<splitedFunctionCall.length;i++)
			{
				functionCallSeperated.add(splitedFunctionCall[i]);
				//System.out.println("Word: "+splitedFunctionCall[i]);
				if(i+1<splitedFunctionCall.length)
					functionCallSeperated.add(".");
			}
			sCurrentToken = functionCallSeperated.get(0);
			
		}
		enCurrentType = typeRead.WORD;
	}

	public String stringVal()
	{
		return sCurrentToken;
	}

	
	public JackCompiler.Keyword keyWord()
	{
	
		return enCurrentKeyword;
	}
	 
	public char symbol()
	{
		return sCurrentToken.charAt(0);
	}

	public String identifier()
	{
		return sCurrentToken;
	}

	public int intVal()
	{
		return Integer.parseInt(sCurrentToken);
	}
}
