import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;


public class SymbolTable {
	public SymbolTable()
	{
		ClassScope = new Hashtable<>();
		SubroutineScope = new Hashtable<>();
		_runStatic=0;
		_runField=0;
		_runARG=0;
		_runVAR=0;
	}
	private int _runStatic;
	private int _runField;
	private int _runARG;
	private int _runVAR;
	class identifier{
		String _sType;
		JackCompiler.Kind _KmyKind;
		int _iRunningIndex;
		 identifier(String type, JackCompiler.Kind kind, int runningIndex)
		{
			 _sType=type;
			 _KmyKind = kind;
			 _iRunningIndex = runningIndex;
		}
	}
	private Hashtable<String, identifier> ClassScope ;//= new Hashtable();
	private Hashtable<String, identifier> SubroutineScope ;
	public void startSubroutine(JackCompiler.Keyword subroutineType)
	{
		SubroutineScope = new Hashtable<>();
		_runARG = 0;
		_runVAR = 0;
		if (subroutineType == JackCompiler.Keyword.METHOD) {
			_runARG++;
		}
	}
	
	public void define(String name, String type, JackCompiler.Kind kind)
	{

		identifier id = null;
		switch (kind)
		{
		case STATIC:
			
			 id = new identifier(type, kind, _runStatic);
			_runStatic++;
			ClassScope.put(name, id);
			break;
		case FIELD:
			 id = new identifier(type, kind, _runField);
			 _runField++;
			ClassScope.put(name, id);
			break;
		case VAR:
			 id = new identifier(type, kind, _runVAR);
			 _runVAR++;
			SubroutineScope.put(name, id);
			break;
		case ARG:
			 id = new identifier(type, kind, _runARG);
			 _runARG++;
			SubroutineScope.put(name, id);
			break;
		}
		//System.out.println("        index of: " + id._iRunningIndex);
		
	}
	

	public int varCount(JackCompiler.Kind kind)
	{
		
		Set<String> keys = ClassScope.keySet();
		    //Obtaining iterator over set entries
		    Iterator<String> itr = keys.iterator();
		    String str;
		    int counter =0;
		    //Displaying Key and value pairs
		    while (itr.hasNext()) { 
		       // Getting Key
		       str = itr.next();
		       identifier id = ClassScope.get(str);
		       if (id._KmyKind==kind)
		    	   counter++;
		    }
		    
			Set<String> keys2 = SubroutineScope.keySet();
		    //Obtaining iterator over set entries
		    Iterator<String> itr2 = keys2.iterator();
		    //Displaying Key and value pairs
		    while (itr2.hasNext()) { 
		       // Getting Key
		       str = itr2.next();
		       identifier id = SubroutineScope.get(str);
		       if (id._KmyKind==kind)
		    	   counter++;
		    }
		    return counter;
	}
	
	public JackCompiler.Kind kindOf(String name)
	{
		identifier id = ClassScope.get(name);;
		if(id !=null){
			return id._KmyKind;
		}
		id = SubroutineScope.get(name);
		if(id !=null){
			return id._KmyKind;
		}
		return JackCompiler.Kind.NONE;
	}
	
	public String typeOf(String name)
	{
		identifier id = ClassScope.get(name);;
		if(id !=null){
			return id._sType;
		}
		id = SubroutineScope.get(name);
		return id._sType;
	}
	
	public int indexOf(String name)
	{
		identifier id = ClassScope.get(name);;
		if(id !=null){
			return id._iRunningIndex;
		}
		id = SubroutineScope.get(name);
		return id._iRunningIndex;
		}
}
