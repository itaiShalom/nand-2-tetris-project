import java.io.IOException;
import java.io.PrintWriter;










public class CompilationEngine {
	private SymbolTable sTable;
	private String className, currSubroutin, returnType;
	private VMWriter vmw;
	private boolean isIfWithoutElse;
	private String indentation;
	private int indentLevel, ifLableNum, whileLableNum;
	private JackTokenizer input;
	private PrintWriter output;
	public CompilationEngine(JackTokenizer inputTokenizer, PrintWriter vmOutput) throws IOException {
		this.isIfWithoutElse = false;
		this.input = inputTokenizer;
		this.output = vmOutput;
		
		this.vmw = new VMWriter(vmOutput); 
		this.indentation = "";
		this.indentLevel = 0;
		this.ifLableNum = 0;
		this.whileLableNum = 0;
		this.className = "";
		this.currSubroutin = "";
		this.returnType = "";
		sTable = new SymbolTable();
	}
	
	public void compileClass() throws IOException {
		//first word should be "class"
		if(input.hasMoreTokens()){
			this.input.advance();
			if(input.tokenType()== JackCompiler.TokenType.KEYWORD && 
					input.keyWord() == JackCompiler.Keyword.CLASS){
				inceaseIndentation();
				//output.println(indentation + "<keyword> class </keyword>");
			}else {
				
			}
			
			// the class name
			this.input.advance();
			if(input.tokenType()== JackCompiler.TokenType.IDENTIFIER && 
					input.keyWord() == JackCompiler.Keyword.CLASS){
				//output.println(indentation + "<identifier> " + input.identifier() + " </identifier>");
				className = input.identifier();
			}
			// the symbol '{'
			this.input.advance();
			if(input.tokenType() == JackCompiler.TokenType.SYMBOL
					&& input.symbol() == '{'){
				//output.println(indentation + "<symbol> { </symbol>");
			}
			//
			if(input.hasMoreTokens()){
				input.advance();
			}
			while (input.tokenType() == JackCompiler.TokenType.KEYWORD 
					&& (input.keyWord() == JackCompiler.Keyword.STATIC || input.keyWord() == JackCompiler.Keyword.FIELD) ) {
				compileClassVarDec();
				
				// advancing to the methods
				//if(input.hasMoreTokens()){
					//input.advance();
				//}
			}
			while (input.tokenType() == JackCompiler.TokenType.KEYWORD  &&
					(input.keyWord() == JackCompiler.Keyword.CONSTRUCTOR || 
					input.keyWord() == JackCompiler.Keyword.METHOD
					|| input.keyWord() == JackCompiler.Keyword.FUNCTION)) {
				CompileSubroutine();
				input.advance();//to the next method or the end of the class
			}
			//advancing to the symbol '}'
			if(input.hasMoreTokens()){
				input.advance();
			}
			if (input.tokenType() == JackCompiler.TokenType.SYMBOL
					&& input.symbol() == '}') {
				//output.println(indentation + "<symbol> } </symbol>");
				decreaseIndentation();
				//output.println(indentation + "</class>");
				
				
			}	
		}
		vmw.close();
	}
	private void compileClassVarDec() throws IOException {
		String type, name;
		type = "";
		name = "";
		JackCompiler.Kind kind;
		//the token is already "static" or "field"
		//output.println(indentation+"<classVarDec>");
		inceaseIndentation();
		if(input.keyWord() == JackCompiler.Keyword.FIELD){
			//output.println(indentation+"<keyword> field </keyword>");
			kind = JackCompiler.Kind.FIELD;
		}else {
			//output.println(indentation+"<keyword> static </keyword>");
			kind = JackCompiler.Kind.STATIC;
		}
		//the type
		input.advance();
		if(input.tokenType() == JackCompiler.TokenType.IDENTIFIER){
			type = input.identifier();
			// its a class name
			//output.println(indentation+"<identifier> "+input.identifier()+ "</identifier>");
		}else if (input.tokenType() == JackCompiler.TokenType.KEYWORD) {
			if(input.keyWord() == JackCompiler.Keyword.INT){
				type = "int";
				//output.println(indentation +"<keyword> int </keyword>");
			}else if (input.keyWord() == JackCompiler.Keyword.CHAR) {
				type = "char";
				//output.println(indentation +"<keyword> char </keyword>");
			}else {
				type = "boolean";
				//output.println(indentation +"<keyword> boolean </keyword>");
			}
		}
		//variables names
		input.advance();
		while (!((input.tokenType() == JackCompiler.TokenType.SYMBOL) && (input.symbol() == ';'))) {
			if(input.tokenType() == JackCompiler.TokenType.IDENTIFIER){
				//output.println(indentation +"<identifier> " + input.identifier() + " </identifier>");
				name = input.identifier();
				
				//System.out.println("defining " + name +" type of " + type + " kind of" + kind );
				sTable.define(name, type, kind);
				input.advance();
				continue;
			}else if (input.tokenType() == JackCompiler.TokenType.SYMBOL 
					&& input.symbol() == ',') {
				//output.println(indentation +"<symbol> , </symbol>");
				input.advance();
				continue;
			}
		}
		//output.println(indentation +"<symbol> ; </symbol>");
		//indentation = oldIndentation;
		decreaseIndentation();
		//output.println(indentation +"</classVarDec>");
		input.advance(); // if problem remove and undocoment the advance in compileClass
	}
	private void CompileSubroutine() throws IOException {
		ifLableNum = 0;
		whileLableNum = 0;
		//String returnType = "";
		JackCompiler.Keyword subroutineKeyword;
		//output.println(indentation +"<subroutineDec>");
		inceaseIndentation();
		subroutineKeyword = input.keyWord();
		sTable.startSubroutine(subroutineKeyword);
		//output.print(indentation +"<keyword> ");
		switch (input.keyWord()) {
		case CONSTRUCTOR:
			//output.print("constructor ");
			break;
		case METHOD:
			//output.print("method ");
			break;
		case FUNCTION:
			//output.print("function ");
			break;
		default:
			break;
		}
		//output.print("</keyword>\n");
		
		//the subroutine's return type
		input.advance();
		if(input.tokenType() == JackCompiler.TokenType.IDENTIFIER){
			// it's non primitive type
			returnType = input.identifier();
			//output.println(indentation +"<identifier> " +input.identifier() + " </identifier>");
		}else{
			switch (input.keyWord()) {
			case VOID:
				//output.println(indentation +"<keyword> void </keyword>");
				returnType = "void";
				break;
			case INT:
				returnType = "int";
				break;
				//output.println(indentation +"<keyword> int </keyword>");
			case CHAR:
				returnType = "char";
				break;
				//output.println(indentation +"<keyword> char </keyword>");
			case BOOLEAN:
				returnType = "boolean";
				break;
				//output.println(indentation +"<keyword> boolean </keyword>");
			default:
				//returnType = "";
				break;
			}
		}
		//the subroutine's name
		input.advance();
		currSubroutin = className + "." + input.identifier();
		//System.out.println("compiling subroutine " + currSubroutin);
		//output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
		input.advance();//to the symbol '{'
		if(input.tokenType() == JackCompiler.TokenType.SYMBOL && input.symbol() == '('){
			compileParameterList();
		}
		
		input.advance();//to the symbol '{'
		if(input.tokenType() == JackCompiler.TokenType.SYMBOL && input.symbol() == '{'){
			CompileSubroutineBody(subroutineKeyword, returnType);
		}
		decreaseIndentation();
		//output.println(indentation +"</subroutineDec>");
	}
	private void CompileSubroutineBody(JackCompiler.Keyword subroutineKeyword, String returnType) throws IOException {
		//output.println(indentation +"<subroutineBody>");
		inceaseIndentation();
		//output.println(indentation +"<symbol> { </symbol>");
		input.advance();
		
		// all the variable decelerations
		while (input.tokenType() == JackCompiler.TokenType.KEYWORD && 
				input.keyWord() == JackCompiler.Keyword.VAR) {
			compileVarDec();
			//move to the next deceleration/statements
			input.advance();
		}
		vmw.writeFunction(currSubroutin, sTable.varCount(JackCompiler.Kind.VAR));
		switch (subroutineKeyword) {
		case CONSTRUCTOR:
			int num = sTable.varCount(JackCompiler.Kind.FIELD);
            if(num <= 0){
                break;
            }
			vmw.writePush(JackCompiler.Segment.CONST, sTable.varCount(JackCompiler.Kind.FIELD));
			vmw.writeCall("Memory.alloc", 1);
			vmw.writePop(JackCompiler.Segment.POINTER, 0);
			
			break;
		case METHOD:
			vmw.writePush(JackCompiler.Segment.ARG, 0);
			vmw.writePop(JackCompiler.Segment.POINTER, 0);
			break;
		default:
			break;
		}

		compileStatements();
		if(input.tokenType() == JackCompiler.TokenType.SYMBOL
				&& input.symbol() == '}'){
			//output.println(indentation +"<symbol> } </symbol>");
			decreaseIndentation();
			//output.println(indentation +"</subroutineBody>");
			
		}
			//the end of the subroutine
			
	}

	private void compileParameterList() throws IOException {
		//output.println(indentation +"<symbol> ( </symbol>");
		//output.println(indentation +"<parameterList>");
		String type, name ;
		type = "";
		name = "";
		
		inceaseIndentation();
		do {
			input.advance();
			if(input.tokenType() == JackCompiler.TokenType.SYMBOL && input.symbol() == ')'){
				break;
			}
			switch (input.tokenType()) {
				case IDENTIFIER:
					type = input.identifier();
					//output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
					break;
				case KEYWORD:
					switch (input.keyWord()) {
					case INT:
						type = "int";
						//output.println(indentation +"<keyword> int </keyword>");
						break;
					case CHAR:
						type = "char";
						break;
						//output.println(indentation +"<keyword> char </keyword>");
					case BOOLEAN:
						type = "boolean";
						break;
						//output.println(indentation +"<keyword> boolean </keyword>");
					default:
						break;
					}
					break;
				default:
					break;
			}
			input.advance();
			name = input.identifier();
			
			sTable.define(name, type, JackCompiler.Kind.ARG);
			input.advance();
			
		} while (!(input.tokenType() == JackCompiler.TokenType.SYMBOL && input.symbol() == ')'));
		decreaseIndentation();
		//output.println(indentation +"</parameterList>");
		//output.println(indentation +"<symbol> ) </symbol>");
	}
	private void compileVarDec() throws IOException {
		String name = "", type = "";
		JackCompiler.Kind kind  = JackCompiler.Kind.VAR;
		//output.println(indentation +"<varDec>");
		inceaseIndentation();
		//the token is already checked to be the keyword "var"
		//output.println(indentation +"<keyword> var </keyword>");
		//the type
		input.advance();
		if (input.tokenType() == JackCompiler.TokenType.KEYWORD) {
			if(input.keyWord() == JackCompiler.Keyword.INT){
				type = "int";
				//output.println(indentation +"<keyword> int </keyword>");
			}else if (input.keyWord() == JackCompiler.Keyword.CHAR) {
				type = "char";
				//output.println(indentation +"<keyword> char </keyword>");
			}else if (input.keyWord() == JackCompiler.Keyword.BOOLEAN){
				type = "boolean";
				//output.println(indentation +"<keyword> boolean </keyword>");
			}
		}
		else if(input.tokenType() == JackCompiler.TokenType.IDENTIFIER){
			// its a class name
			type = input.identifier();
			//output.println(indentation+"<identifier> "+input.identifier()+ " </identifier>");
			
		}
		
		//variables names
		input.advance();
		while (!((input.tokenType() == JackCompiler.TokenType.SYMBOL) && (input.symbol() == ';'))) {
			if(input.tokenType() == JackCompiler.TokenType.IDENTIFIER){
				name = input.identifier();
				sTable.define(name, type, kind);
				//output.println(indentation +"<identifier> " + input.identifier() + " </identifier>");
				input.advance();
				continue;
			}else if (input.tokenType() == JackCompiler.TokenType.SYMBOL 
					&& input.symbol() == ',') {
				//output.println(indentation +"<symbol> , </symbol>");
				input.advance();
				continue;
			}
		}
		
		//output.println(indentation +"<symbol> ; </symbol>");
		decreaseIndentation();
		//output.println(indentation +"</varDec>");
		
	}
	private void compileStatements() throws IOException {
		//output.println(indentation +"<statements>");
		inceaseIndentation();
		while (!(input.tokenType() == JackCompiler.TokenType.SYMBOL
				&& input.symbol() == '}')) {
			if(input.tokenType() == JackCompiler.TokenType.KEYWORD){
				switch (input.keyWord()) {
				case LET:
					compileLet();
					break;
				case IF:
					compileIf();
					break;
				case WHILE:
					compileWhile();
					break;
				case DO:
					compileDo();
					break;
				case RETURN:
					compileReturn();
					break;

				default:
					break;
				}
			}
			// if the compileIf read the next word and it was'nt "else", skip the next read:
			if(!isIfWithoutElse){
				input.advance();
			}
			isIfWithoutElse = false;
			
		}
		decreaseIndentation();
		//output.println(indentation +"</statements>");
	}
	private void compileDo() throws IOException {
		String calledSubroutine = "", tempString = "";
		JackCompiler.Segment segment; 
		boolean isMethod = false;
		int nArgs = 0;
		//output.println(indentation +"<doStatement>");
		inceaseIndentation();
		//the token is already the keyword "do"
		//output.println(indentation +"<keyword> do </keyword>");
		input.advance();
		if(input.tokenType() == JackCompiler.TokenType.IDENTIFIER){
			//output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
			tempString = input.identifier();
			input.advance();// to a symbol - '.' or '('
			if(input.tokenType() == JackCompiler.TokenType.SYMBOL){
				switch (input.symbol()) {
				case '(':
					calledSubroutine = className + "." + tempString;
					segment = JackCompiler.Segment.POINTER;
					vmw.writePush(segment, 0);
					isMethod = true;
					//output.println(indentation +"<symbol> ( </symbol>");
					nArgs = CompileExpressionList();
					break;
				case '.': // it's method/function of other class
					//tempString = calledSubroutine;
					JackCompiler.Kind kind = sTable.kindOf(tempString);
					if(kind != JackCompiler.Kind.NONE){
						// assuming that the jack file is legal, it must be a method call
						isMethod = true;
						switch (kind) {
						case STATIC:
							segment = JackCompiler.Segment.STATIC;
							break;
						case VAR:
							segment = JackCompiler.Segment.LOCAL;
							break;
						case ARG:
							segment = JackCompiler.Segment.ARG;
							break;
						case FIELD:
							segment = JackCompiler.Segment.THIS;
							break;
						default:
							segment = null;
							break;
						}
						vmw.writePush(segment, sTable.indexOf(tempString));
						
						
					}
					//output.println(indentation +"<symbol> . </symbol>");
					input.advance();
					if(input.tokenType() == JackCompiler.TokenType.IDENTIFIER){
						//if(input.identifier().equals("printString")){
						//}
						if(isMethod){
							
							calledSubroutine = sTable.typeOf(tempString) + "." + input.identifier();
						}else {
							calledSubroutine = tempString + "." + input.identifier();
						}
						//calledSubroutine += "." + input.identifier();
						//output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
						input.advance(); // to the symbol '('
						if(input.tokenType() == JackCompiler.TokenType.SYMBOL &&
								input.symbol() == '('){
							//output.println(indentation +"<symbol> ( </symbol>");
							nArgs = CompileExpressionList();
						}
					}
					break;
				default:
					break;
				}
			}
			if(input.tokenType() == JackCompiler.TokenType.SYMBOL &&
								input.symbol() == ')'){
				//output.println(indentation +"<symbol> ) </symbol>");
				input.advance(); // to the symbol ';'
				if(input.tokenType() == JackCompiler.TokenType.SYMBOL &&
								input.symbol() == ';'){
					//output.println(indentation +"<symbol> ; </symbol>");
					decreaseIndentation();
					//output.println(indentation +"</doStatement>");
				}
			}

		}
		if(isMethod){
			nArgs++;
		}
		vmw.writeCall(calledSubroutine, nArgs);
		vmw.writePop(JackCompiler.Segment.TEMP, 0);
	}
	private void compileLet() throws IOException {
		String varName = "";
		boolean isArrayEntry = false;
		//this.output.println(indentation + "<letStatement>");
		inceaseIndentation();
		//this.output.println(indentation +"<keyword> let </keyword>");
		this.input.advance();
		if(input.tokenType() ==  JackCompiler.TokenType.IDENTIFIER){
			varName = input.identifier();
			//output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
			input.advance();
		}
		if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL){
			if (input.symbol() == '[') {
				//output.println(indentation +"<symbol> [ </symbol>");
				isArrayEntry = true;
				JackCompiler.Segment segment;
				switch (sTable.kindOf(varName)) {
				case STATIC:
					segment = JackCompiler.Segment.STATIC;
					break;
				case VAR:
					segment = JackCompiler.Segment.LOCAL;
					break;
				case ARG:
					segment = JackCompiler.Segment.ARG;
					break;
				case FIELD:
					segment = JackCompiler.Segment.THIS;
					break;
				default:
					segment = null;
					break;
				}
				vmw.writePush(segment, sTable.indexOf(varName));
				input.advance();
				CompileExpression();
				vmw.WriteArithmetic(JackCompiler.command.ADD);
				 // to the ']' symbol
				if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && input.symbol() == ']'){
					//output.println(indentation +"<symbol> ] </symbol>");
					input.advance(); // to the '='
				}
			}
			if (input.symbol() == '=') {
				//output.println(indentation +"<symbol> = </symbol>");
				input.advance(); // to the expression
				CompileExpression();
			}
		}
		//end of the let statement
		if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && input.symbol() == ';'){
			//output.println(indentation +"<symbol> ; </symbol>");
			decreaseIndentation();
			//output.println(indentation +"</letStatement>");
		}
		if(isArrayEntry){
			vmw.writePop(JackCompiler.Segment.TEMP, 0);
			vmw.writePop(JackCompiler.Segment.POINTER, 1);
			vmw.writePush(JackCompiler.Segment.TEMP, 0);
			vmw.writePop(JackCompiler.Segment.THAT, 0);
					
		}else{
			vmw.writePop(segmentOfKind(sTable.kindOf(varName)), sTable.indexOf(varName));
		}
	}
	private JackCompiler.Segment segmentOfKind(JackCompiler.Kind kind) {
		switch (kind) {
		case STATIC:
			return JackCompiler.Segment.STATIC;
		case VAR:
			return JackCompiler.Segment.LOCAL;
		case ARG:
			return JackCompiler.Segment.ARG;
		case FIELD:
			return JackCompiler.Segment.THIS;
		default:
			break;
		}
		return null;
	}

	private void compileWhile() throws IOException {
		int lableId = whileLableNum;
		whileLableNum++;
		vmw.WriteLabel(currSubroutin + "WHILE_" + lableId);
		//output.println(indentation +"<whileStatement>");
		inceaseIndentation();
		// the token is already the keyword "while"
		//output.println(indentation +"<keyword> while </keyword>");
		input.advance(); // to the symbol '('
		if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && input.symbol() == '('){
			//output.println(indentation +"<symbol> ( </symbol>");
			input.advance(); // to the expression
			CompileExpression();
			vmw.WriteArithmetic(JackCompiler.command.NOT);
			vmw.WriteIf(currSubroutin + "WHILE_END" + lableId);
			if (input.tokenType() ==  JackCompiler.TokenType.SYMBOL && input.symbol() == ')') {
				//output.println(indentation +"<symbol> ) </symbol>");
				input.advance(); //to the symbol '{'
				if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && input.symbol() == '{'){
					//output.println(indentation +"<symbol> { </symbol>");
					//input.advance(); // to the statements
					compileStatements();
					vmw.WriteGoto(currSubroutin + "WHILE_" + lableId);
					if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && input.symbol() == '}'){
						//output.println(indentation +"<symbol> } </symbol>");
					}
				}
				
			}
		}
		vmw.WriteLabel(currSubroutin + "WHILE_END" + lableId);
		decreaseIndentation();
		//whileLableNum++;
		//output.println(indentation +"</whileStatement>");
		
		
	}
	private void compileReturn() throws IOException {
		//output.println(indentation +"<returnStatement>");
		inceaseIndentation();
		// the token is already the keyword "return"
		//output.println(indentation +"<keyword> return </keyword>");
		input.advance(); //to the expression or the symbol ';'
		if(!(input.tokenType() == JackCompiler.TokenType.SYMBOL && input.symbol() == ';')){
			CompileExpression();
			
		}else{
			vmw.writePush(JackCompiler.Segment.CONST, 0);
		}
		vmw.writeReturn();
		//output.println(indentation +"<symbol> ; </symbol>");
		decreaseIndentation();
		//output.println(indentation +"</returnStatement>");
	}
	private void compileIf() throws IOException {
		int lablaeNum = ifLableNum;
		ifLableNum++;
		//output.println(indentation +"<ifStatement>");
		inceaseIndentation();
		//output.println(indentation +"<keyword> if </keyword>");
		input.advance(); // to the symbol '('
		//output.println(indentation +"<symbol> ( </symbol>");
		input.advance(); // to the expression
		CompileExpression();
		vmw.WriteArithmetic(JackCompiler.command.NOT);
		vmw.WriteIf(currSubroutin + "IF_FALSE_"+lablaeNum);
		if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && input.symbol() == ')'){
			//output.println(indentation +"<symbol> ) </symbol>");
			input.advance(); //to the symbol '{'
		}
		if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && input.symbol() == '{'){
			//output.println(indentation +"<symbol> { </symbol>");
			input.advance(); // to the statements
			compileStatements();
		}
		if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && input.symbol() == '}'){
			//output.println(indentation +"<symbol> } </symbol>");
			input.advance();
			if(!(input.tokenType() ==  JackCompiler.TokenType.KEYWORD &&
					input.keyWord() == JackCompiler.Keyword.ELSE)){
				// we read one token to far since this "if" does'nt have "else" part 
				isIfWithoutElse = true;
				decreaseIndentation();
				vmw.WriteGoto(currSubroutin + "IF_FALSE_"+lablaeNum);
				vmw.WriteLabel(currSubroutin + "IF_FALSE_"+lablaeNum);
				//output.println(indentation +"</ifStatement>");
				return;
			}else{
				//there is an "else" part
				vmw.WriteGoto(currSubroutin + "IF_TRUE_"+lablaeNum);
				vmw.WriteLabel(currSubroutin + "IF_FALSE_"+lablaeNum);
				//output.println(indentation +"<keyword> else </keyword>");
				input.advance(); // to the symbol '{'
				if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL &&
						input.symbol() == '{'){
					//output.println(indentation +"<symbol> { </symbol>");
					input.advance(); // to the statements
					compileStatements();
					vmw.WriteLabel(currSubroutin + "IF_TRUE_"+lablaeNum);
				}
				if(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && 
						input.symbol() == '}'){
					//output.println(indentation +"<symbol> } </symbol>");
					decreaseIndentation();
					//output.println(indentation +"</ifStatement>");
				}				
			}
		}
	}
	
	private void CompileExpression() throws IOException {
		//output.println(indentation +"<expression>");
		inceaseIndentation();
		CompileTerm();
		while(isOp()){
			char op = input.symbol();
			//output.println(indentation +"<symbol> " +symbolCheck(input.symbol()) +" </symbol>");
			input.advance(); //to the next term
			CompileTerm();
			writeOp(op);
		}
		decreaseIndentation();
		//output.println(indentation +"</expression>");
	}
	private void writeOp(char op) {
		switch (op) {
		case '+':
			vmw.WriteArithmetic(JackCompiler.command.ADD);
			break;
		case '-':
			vmw.WriteArithmetic(JackCompiler.command.SUB);
			break;
		case '&':
			vmw.WriteArithmetic(JackCompiler.command.AND);
			break;
		case '|':
			vmw.WriteArithmetic(JackCompiler.command.OR);
			break;
		case '=':
			vmw.WriteArithmetic(JackCompiler.command.EQ);
			break;
		case '<':
			vmw.WriteArithmetic(JackCompiler.command.LT);
			break;
		case '>':
			vmw.WriteArithmetic(JackCompiler.command.GT);
			break;
		case '*':
			vmw.writeCall("Math.multiply",2);
			break;
		case '/':
			vmw.writeCall("Math.divide", 2);
			break;
		default:
			break;
		}
		
	}

	private boolean isOp() {
		if(input.tokenType() == JackCompiler.TokenType.SYMBOL){
			char s = input.symbol();
			if((s == '+')
				||(s == '-')
				||(s == '*')
				||(s == '/')
				||(s == '&')
				||(s == '|')
				||(s == '<')
				||(s == '>')
				||(s == '=')){
				return true;
			}
		}
		return false;
	}

	private void CompileTerm() throws IOException {
		//output.println(indentation +"<term>");
		inceaseIndentation();
		if(input.tokenType() == JackCompiler.TokenType.INT_CONST){
			//int val = input.intVal();
			vmw.writePush(JackCompiler.Segment.CONST, input.intVal());
			//if(val < 0){
				//vmw.WriteArithmetic(JackCompiler.command.NEG);
				//System.out.println("term  = '-1' ");
				
			//}
			//output.println(indentation +"<integerConstant> "+input.intVal()+" </integerConstant>");
			input.advance();
		}else if (input.tokenType() == JackCompiler.TokenType.STRING_CONST) {
			String constnat = stringCheck(input.stringVal());
			writeConstString(constnat);
			//output.println(indentation +"<stringConstant> "+stringCheck(input.stringVal())+" </stringConstant>");
			input.advance();
		}else if (input.tokenType() == JackCompiler.TokenType.KEYWORD) {
			writeConstantKeyword();
			//output.println(indentation +"<keyword> "+constantKeyword()+" </keyword>");
			input.advance();
		}else if (input.tokenType() == JackCompiler.TokenType.SYMBOL) {
			//output.println(indentation +"<symbol> "+symbolCheck(input.symbol())+" </symbol>");
			if(input.symbol() == '-' || input.symbol() == '~'){
				char unarOp = input.symbol();
				input.advance(); // to the term after the unaryOp
				CompileTerm();
				if(unarOp == '-'){
					vmw.WriteArithmetic(JackCompiler.command.NEG);
				}else{
					vmw.WriteArithmetic(JackCompiler.command.NOT);
				}
			}else if (input.symbol() == '(') {
				input.advance(); // to the expression
				CompileExpression();
				if(input.tokenType() == JackCompiler.TokenType.SYMBOL
						&& input.symbol() == ')'){
					//output.println(indentation +"<symbol> ) </symbol>");
					input.advance();
				}
			}
		}else if (input.tokenType() == JackCompiler.TokenType.IDENTIFIER) {
			String identirier = input.identifier();
			boolean compiled = false;
			//output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
			input.advance();
			if(input.tokenType() == JackCompiler.TokenType.SYMBOL ){
				if(input.symbol() == '['){
					compiled = true;
					//it's an array entry
					vmw.writePush(segmentOfKind(sTable.kindOf(identirier)), sTable.indexOf(identirier));
					//output.println(indentation +"<symbol> "+symbolCheck(input.symbol())+" </symbol>");
					input.advance();
					CompileExpression();
					if(input.tokenType() == JackCompiler.TokenType.SYMBOL
							&& input.symbol() == ']'){
						vmw.WriteArithmetic(JackCompiler.command.ADD);
						vmw.writePop(JackCompiler.Segment.POINTER, 1);
						vmw.writePush(JackCompiler.Segment.THAT, 0);
						//output.println(indentation +"<symbol> ] </symbol>");
						input.advance();
					}
				}else if (input.symbol() == '(') {
					compiled = true;
					// its a call for inner method
					vmw.writePush(JackCompiler.Segment.THIS, 0);
					//output.println(indentation +"<symbol> "+symbolCheck(input.symbol())+" </symbol>");
					int nArgs = CompileExpressionList() + 1;
					if (input.tokenType() == JackCompiler.TokenType.SYMBOL
						&& input.symbol() == ')') {
						//output.println(indentation +"<symbol> ) </symbol>");
						vmw.writeCall(className + "." + identirier, nArgs);
						input.advance();
					}
					
				}else if (input.symbol() == '.') {
					compiled = true;
					int nArgs = 0;
					boolean isMethod = false;
					JackCompiler.Kind kind = sTable.kindOf(identirier);
					if(kind != JackCompiler.Kind.NONE){
						/* 
						 * assuming the jack file is legal, 
						 * identifier must be a non-primitive type with a method 
						 * who's name is the next identifier 
						 *
						 */
						isMethod = true;
						vmw.writePush(segmentOfKind(kind), sTable.indexOf(identirier));
						
					}
					//output.println(indentation +"<symbol> "+symbolCheck(input.symbol())+" </symbol>");
					input.advance();
					if(input.tokenType() == JackCompiler.TokenType.IDENTIFIER){
						String functionName = input.identifier();
						//output.println(indentation +"<identifier> "+input.identifier()+" </identifier>");
						input.advance(); // to the symbol '('
						if(input.tokenType() == JackCompiler.TokenType.SYMBOL &&
								input.symbol() == '('){
							//output.println(indentation +"<symbol> ( </symbol>");
							nArgs = CompileExpressionList();
							if(isMethod){
								nArgs++;
								functionName = sTable.typeOf(identirier) + "." + functionName;
							}else{
								functionName = identirier + "." + functionName;
							}
							
							if(input.tokenType() == JackCompiler.TokenType.SYMBOL &&
									input.symbol() == ')'){
								//output.println(indentation +"<symbol> ) </symbol>");
								vmw.writeCall(functionName, nArgs);
								input.advance();
							}
						}
					}
					
				}
				
			}
			if(!compiled){
				vmw.writePush(segmentOfKind(sTable.kindOf(identirier)), sTable.indexOf(identirier));
				
				
			}
		}
		decreaseIndentation();
		//output.println(indentation +"</term>");
		
	}


	private void writeConstString(String constnat) {
		// TODO Auto-generated method stub
		vmw.writePush(JackCompiler.Segment.CONST, constnat.length());
		vmw.writeCall("String.new", 1);
		for (int i = 0; i < constnat.length(); i++) {
			vmw.writePush(JackCompiler.Segment.CONST, charToASCII(constnat.charAt(i)));
			vmw.writeCall("String.appendChar", 2);
		}
		
	}

	private int charToASCII(char charAt) {
		return (int)charAt;
	}

	private String writeConstantKeyword() {
		switch (input.keyWord()) {
		case TRUE:
			vmw.writePush(JackCompiler.Segment.CONST, 1);
			vmw.WriteArithmetic(JackCompiler.command.NEG);
			break;
		case FALSE:
			vmw.writePush(JackCompiler.Segment.CONST, 0);
			break;
			//return "false";
		case NULL:
			vmw.writePush(JackCompiler.Segment.CONST, 0);
			break;
			//return "null";
		case THIS:
			vmw.writePush(JackCompiler.Segment.POINTER, 0);
			break;
			//return "this";

		default:
			break;
		}
		return "WTF?"; // for debug
	}

	private int CompileExpressionList() throws IOException {
		int nArgs = 0;
		//output.println(indentation +"<expressionList>");
		inceaseIndentation();
		input.advance();
		if((input.tokenType() ==  JackCompiler.TokenType.SYMBOL && 
				input.symbol() == ')')){
			decreaseIndentation();
			//output.println(indentation +"</expressionList>");
			return nArgs; //to do
		}else{
			nArgs++;
			do {
				CompileExpression();
				if (input.tokenType() ==  JackCompiler.TokenType.SYMBOL && 
						input.symbol() == ',') {
					nArgs++;
					//output.println(indentation +"<symbol> , </symbol>");
					input.advance(); // to the next expression
				}
				
			} while (!(input.tokenType() ==  JackCompiler.TokenType.SYMBOL && 
					input.symbol() == ')'));
			decreaseIndentation();
			//output.println(indentation +"</expressionList>");
			return nArgs; // to do
		}
	}
	/*
	 * handle special characters in the xml format
	 */
	private String symbolCheck(char symbol){
		switch (symbol) {
		case '<':
			return "&lt;";
		case '>':
			return "&gt;";
		case '"':
			return "&quot;";
		case '&':
			return "&amp;";

		default:
			return Character.toString(symbol);
		}
	}
	
	private String stringCheck(String stringConst){
		String result = stringConst;
		while(containsEscapeChar(result)){
			result = replceEscapeChars(result);
		}
		result = result.replaceAll("<", "&lt;");
		result = result.replaceAll(">", "&gt;");
		result = result.replaceAll("\"", "&quot;");
		result = result.replaceAll("&", "&amp;");
		return result;
	}
	private String replceEscapeChars(String result) {
		result = result.replace("\t", "\\t");
		result = result.replace("\n", "\\n");
		result = result.replace("\r", "\\r");
		result = result.replace("\f", "\\f");
		result = result.replace("\'", "\\'");
		return result;
	}

	private boolean containsEscapeChar(String result) {
		if(result.contains("\t") || result.contains("\n")|| result.contains("\r")
				||result.contains("\f")
				//||result.contains("\'") 
				|| result.contains("\"")){
				// result.contains("\\")){
			return true;
		}
		return false;
	}


	private void inceaseIndentation(){
		indentLevel++;
		indentation ="";
		for (int i = 0; i < indentLevel; i++) {
			indentation += "  ";
		}
	}
	private void decreaseIndentation(){
		indentLevel--;
		indentation ="";
		for (int i = 0; i < indentLevel; i++) {
			indentation += "  ";
		}
	}

}
