import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class JackCompiler {

	public enum Kind {
		STATIC, FIELD,ARG,VAR, NONE
	}
	
	public enum  command {ADD, SUB,
			NEG, EQ, GT, LT,
			AND, OR, NOT}
	
	public enum Segment {CONST,
			ARG, LOCAL,
			STATIC, THIS,
			THAT, POINTER,
			TEMP}
			
	public enum TokenType{
		KEYWORD, SYMBOL, IDENTIFIER, INT_CONST, STRING_CONST
	}
	public enum Keyword{
		CLASS, METHOD, FUNCTION, CONSTRUCTOR, INT, BOOLEAN, CHAR, VOID,
		VAR, STATIC, FIELD, LET, DO, IF, ELSE, WHILE, RETURN, TRUE, FALSE,
		NULL, THIS
	}
	
	
	public static void main(String[] args) throws IOException 
	{
		File file = new File(args[0]);
		if(file.isFile()){
			// assuming valid input
			driver(file);
		}else{
			// again, assuming valid input
			File[] filesList = file.listFiles();
			for (int i = 0; i < filesList.length; i++) {
				if(filesList[i].isFile() && filesList[i].getAbsolutePath().endsWith(".jack")){
					driver(filesList[i]);
				}
			}
		}
	}

	/*
	private static void driver(File f) throws FileNotFoundException, IOException

	{

		JackTokenizer tokenizer = new JackTokenizer(new BufferedReader(new FileReader(f.getAbsolutePath())));

		while(true)

		{

		tokenizer.advance();

		if(!tokenizer.hasMoreTokens()){

			break;
		}
		TokenType enToken = tokenizer.tokenType();

		switch (enToken)

		{

		case SYMBOL:

			char c = tokenizer.symbol();

			System.out.println("SYMBOL: "+c);

			break;

		case KEYWORD:

			Keyword enkeyWord = tokenizer.keyWord();

			System.out.println("KEYWORD: "+enkeyWord);

			break;

		case IDENTIFIER:

			String sIdentifierString = tokenizer.identifier();

			System.out.println("IDENTIFIER: "+sIdentifierString);

			break;

		case INT_CONST:

			int number = tokenizer.intVal();

			System.out.println("INT_CONST: "+number);

			break;

		case STRING_CONST:

			String constString = tokenizer.stringVal();

			System.out.println("STRING_CONST: "+constString);

			break;

		}

		//System.out.println(tokenizer.tokenType()+": "+tokenizer.stringVal());

	}

	}

	/*

		File file = new File(args[0]);

		if(file.isFile()){

			// assuming valid input

			driver(file);

		}else{

			// again, assuming valid input

			File[] filesList = file.listFiles();

			for (int i = 0; i < filesList.length; i++) {

				if(filesList[i].isFile() && filesList[i].getAbsolutePath().endsWith(".jack")){

					driver(filesList[i]);

				}

			}

		}

	 
	/**
	*yonatan's drivaer
	*/
	
	private static void driver(File file) throws IOException{

		// TODO Auto-generated method stub
		BufferedReader bf = new BufferedReader(new FileReader(file));
		JackTokenizer tokenizer = new JackTokenizer(bf);
		

		String vmFile = file.getAbsolutePath().replace(".jack", ".vm");

		PrintWriter writer = new PrintWriter(vmFile, "UTF-8");
		//System.out.println("compiling file" + file.getName());
		CompilationEngine engine = new CompilationEngine(tokenizer, writer);

		engine.compileClass();
		bf.close();
		//writer.flush();
		//writer.close();

	}

}

