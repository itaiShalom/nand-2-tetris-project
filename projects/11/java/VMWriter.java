import java.io.IOException;
import java.io.PrintWriter;


public class VMWriter {
	private PrintWriter _sOutputPrinter;
	public VMWriter(PrintWriter sOutputPrinter) throws IOException{
		_sOutputPrinter = sOutputPrinter;
	}
	public void writePush(JackCompiler.Segment segment, int Index)
	{
		writePushPop(segment,Index,"push");
	}

	public void writePop(JackCompiler.Segment segment, int Index)
	{
		writePushPop(segment,Index,"pop");
	}

	private void writePushPop(JackCompiler.Segment segment, int Index,String Command)
	{
		this._sOutputPrinter.println(Command +" "+segmentToString(segment.toString())+" "+Index);	
	}

	public void WriteArithmetic(JackCompiler.command command)
	{
		this._sOutputPrinter.println(command.toString().toLowerCase());	
	}

	public void WriteLabel(String label)
	{
		this._sOutputPrinter.println("label"+" "+label);	
	} 

	public void WriteGoto(String label)
	{
		this._sOutputPrinter.println("goto"+" "+label);	
	} 

	public void WriteIf (String label)
	{
		this._sOutputPrinter.println("if-goto"+" "+label);	
	} 

	public void writeCall(String name, int nArgs)
	{
		this._sOutputPrinter.println("call"+" "+name+" "+nArgs);	
	}

	public void  writeFunction(String name, int nLocals)
	{
		this._sOutputPrinter.println("function"+" "+name+" "+nLocals);	
	}

	public void writeReturn() {
		this._sOutputPrinter.println("return");
	}

	public void close()
	{
		_sOutputPrinter.close();
	}
	
	/*
	 * 	ARG, LOCAL,
			STATIC, THIS,
			THAT, POINTER,
			TEMP}
	 */
	
	private String segmentToString(String seg)
	{
		if ( seg.equals("STATIC")|| seg.equals("THIS")||seg.equals("THAT")||seg.equals("TEMP")
				||seg.equals("LOCAL")||seg.equals("POINTER"))
			return seg.toLowerCase();
		if (seg.equals("CONST"))
			return "constant";
		if (seg.equals("ARG"))
			return "argument";
		return seg;
	}
}
